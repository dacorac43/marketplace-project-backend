package models

type MENSAJESENTREUSUARIOS struct {
	IDREMITENTE    *USUARIOS `orm:"column(ID_REMITENTE);rel(fk)"`
	IDDESTINATARIO *USUARIOS `orm:"column(ID_DESTINATARIO);rel(fk)"`
	MENSAJE        string    `orm:"column(MENSAJE);size(1024)"`
}
