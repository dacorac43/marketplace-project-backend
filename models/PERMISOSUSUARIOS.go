package models

type PERMISOSUSUARIOS struct {
	IDUSUARIO *USUARIOS `orm:"column(ID_USUARIO);rel(fk)"`
	IDPERMISO *PERMISOS `orm:"column(ID_PERMISO);rel(fk)"`
}
