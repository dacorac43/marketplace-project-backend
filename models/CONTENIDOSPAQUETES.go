package models

type CONTENIDOSPAQUETES struct {
	IDCONTENIDO *CONTENIDOS `orm:"column(ID_CONTENIDO);rel(fk)"`
	IDPAQUETE   *PAQUETES   `orm:"column(ID_PAQUETE);rel(fk)"`
}
