package models

import (
	"crypto/sha512"
	"encoding/hex"
	"time"

	"github.com/astaxie/beego/orm"
)

// CalcularHashPassword ...
func CalcularHashPassword(password string) string {
	// Se usa el algoritmo SHA512
	hash := sha512.Sum512([]byte(password))
	return hex.EncodeToString(hash[:])
}

// CalcularApiToken ...
func CalcularApiToken(usuario USUARIOS) string {

	timestamp := time.Now().Format(time.UnixDate)
	valorBase := usuario.CORREO + timestamp
	hash := sha512.Sum512([]byte(valorBase))
	hashValorBase := hex.EncodeToString(hash[:])

	o := orm.NewOrm()
	qs := o.QueryTable(new(USUARIOS))
	for qs.Filter("API_TOKEN", hashValorBase).Exist() {
		valorBase = valorBase + "1"
		hash = sha512.Sum512([]byte(valorBase))
		hashValorBase = hex.EncodeToString(hash[:])
	}

	return hashValorBase
}
