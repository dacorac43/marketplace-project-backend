package models

type ADMINISTRADORESFOROS struct {
	IDFORO  *FOROS           `orm:"column(ID_FORO);rel(fk)"`
	IDADMIN *ADMINISTRADORES `orm:"column(ID_ADMIN);rel(fk)"`
}
