package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/astaxie/beego/orm"
)

type SERVICIOS struct {
	Id             int         `orm:"column(ID_SERVICIO);auto"`
	IDPAQUETE      *PAQUETES   `orm:"column(ID_PAQUETE);rel(fk)"`
	IDCATEGORIA    *CATEGORIAS `orm:"column(ID_CATEGORIA);rel(fk)"`
	TITULOSERVICIO string      `orm:"column(TITULO_SERVICIO);size(1024)"`
}

func (t *SERVICIOS) TableName() string {
	return "SERVICIOS"
}

func init() {
	orm.RegisterModel(new(SERVICIOS))
}

type ErrorIntegridadServicio string

func (e ErrorIntegridadServicio) Error() string {
	return string(e)
}

// RevisarIntegridadServicio revisa la integridad de la información
// registro del servicio
func RevisarIntegridadServicios(servicio SERVICIOS, idcategoria int) error {
	o := orm.NewOrm()
	if servicio.TITULOSERVICIO == "" {
		return ErrorIntegridadServicio("El titulo del servicio no debe ser vacio")
	}
	//revisar si la categoría existe
	if !o.QueryTable(new(CATEGORIAS)).Filter("ID_CATEGORIA", idcategoria).Exist() {
		return ErrorIntegridadServicio("La categoría indicada no existe")
	}
	// Retornar nil si no hubo error
	return nil
}

// AddSERVICIOS insert a new SERVICIOS into database and returns
// last inserted Id on success.
func AddSERVICIOS(m *SERVICIOS) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetSERVICIOSById retrieves SERVICIOS by Id. Returns error if
// Id doesn't exist
func GetSERVICIOSById(id int) (v *SERVICIOS, err error) {
	o := orm.NewOrm()
	v = &SERVICIOS{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllSERVICIOS retrieves all SERVICIOS matches certain condition. Returns empty list if
// no records exist
func GetAllSERVICIOS(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(SERVICIOS))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []SERVICIOS
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateSERVICIOS updates SERVICIOS by Id and returns error if
// the record to be updated doesn't exist
func UpdateSERVICIOSById(m *SERVICIOS) (err error) {
	o := orm.NewOrm()
	v := SERVICIOS{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteSERVICIOS deletes SERVICIOS by Id and returns error if
// the record to be deleted doesn't exist
func DeleteSERVICIOS(id int) (err error) {
	o := orm.NewOrm()
	v := SERVICIOS{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&SERVICIOS{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
