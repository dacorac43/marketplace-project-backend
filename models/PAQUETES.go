package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/astaxie/beego/orm"
)

type PAQUETES struct {
	Id                int          `orm:"column(ID_PAQUETE);auto"`
	IDPROVEEDOR       *PROVEEDORES `orm:"column(ID_PROVEEDOR);rel(fk)"`
	DISPONIBILIDAD    int          `orm:"column(DISPONIBILIDAD)"`
	COSTO             float32      `orm:"column(COSTO)"`
	PROMEDIO          float32      `orm:"column(PROMEDIO);null"`
	NUMCALIFICACIONES int          `orm:"column(NUM_CALIFICACIONES)"`
	DESCUENTO         float32      `orm:"column(DESCUENTO);null"`
	TITULOPAQUETE     string       `orm:"column(TITULO_PAQUETE);size(1024)"`
	CIUDADDESTINO     string       `orm:"column(CIUDAD_DESTINO);size(1024);null"`
	CUPOSPUBLICADOS   int          `orm:"column(CUPOS_PUBLICADOS)"`
}

func (t *PAQUETES) TableName() string {
	return "PAQUETES"
}

func init() {
	orm.RegisterModel(new(PAQUETES))
}

// AddPAQUETES insert a new PAQUETES into database and returns
// last inserted Id on success.
func AddPAQUETES(m *PAQUETES) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

type ErrorIntegridadPaquete string

func (e ErrorIntegridadPaquete) Error() string {
	return string(e)
}

// RevisarIntegridadPaquete revisa la integridad de la información
// registro del paquete
func RevisarIntegridadPaquetes(paquete PAQUETES) error {
	if paquete.CIUDADDESTINO == "" {
		return ErrorIntegridadPaquete("El destino del paquete no debe ser vacio")
	}
	//revisar si la categoría existe
	if paquete.COSTO <= 0 {
		return ErrorIntegridadPaquete("El costo no puede ser menor o igual a 0")
	}
	if paquete.CUPOSPUBLICADOS <= 0 {
		return ErrorIntegridadPaquete("Los cupos no puede ser menores o iguales a 0")
	}
	if paquete.DISPONIBILIDAD <= 0 {
		return ErrorIntegridadPaquete("La disponibilidad no puede ser menor o igual a 0")
	}
	// Retornar nil si no hubo error
	return nil
}

// GetPAQUETESById retrieves PAQUETES by Id. Returns error if
// Id doesn't exist
func GetPAQUETESById(id int) (v *PAQUETES, err error) {
	o := orm.NewOrm()
	v = &PAQUETES{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllPAQUETES retrieves all PAQUETES matches certain condition. Returns empty list if
// no records exist
func GetAllPAQUETES(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(PAQUETES))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []PAQUETES
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdatePAQUETES updates PAQUETES by Id and returns error if
// the record to be updated doesn't exist
func UpdatePAQUETESById(m *PAQUETES) (err error) {
	o := orm.NewOrm()
	v := PAQUETES{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeletePAQUETES deletes PAQUETES by Id and returns error if
// the record to be deleted doesn't exist
func DeletePAQUETES(id int) (err error) {
	o := orm.NewOrm()
	v := PAQUETES{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&PAQUETES{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
