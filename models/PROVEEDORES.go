package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/astaxie/beego/orm"
)

type PROVEEDORES struct {
	Id                 int              `orm:"column(ID_PROVEEDOR);pk"`
	IDADMINREGISTRO    *ADMINISTRADORES `orm:"column(ID_ADMIN_REGISTRO);rel(fk)"`
	IDADMINELIMINACION *ADMINISTRADORES `orm:"column(ID_ADMIN_ELIMINACION);rel(fk);null"`
	ORGANIZACION       string           `orm:"column(ORGANIZACION);size(1024);null"`
}

func (t *PROVEEDORES) TableName() string {
	return "PROVEEDORES"
}

func init() {
	orm.RegisterModel(new(PROVEEDORES))
}

// ErrorIntegridadProveedor es un error que sucede cuando la información de solo
// el registro del proveedor no cumple los requisitos esperados
type ErrorIntegridadProveedor string

func (e ErrorIntegridadProveedor) Error() string {
	return string(e)
}

// RevisarIntegridadProveedor revisa la integridad de la información de solo el
// registro del proveedor
func RevisarIntegridadProveedor(proveedor PROVEEDORES) error {
	longitudOrganizacion := len(proveedor.ORGANIZACION)
	if longitudOrganizacion < 5 || longitudOrganizacion > 30 {
		return ErrorIntegridadProveedor("Longitud de la organización debe ser mayor que 4 y menor que 31")
	}
	// Retornar nil si no hubo error
	return nil
}

// AddPROVEEDORES insert a new PROVEEDORES into database and returns
// last inserted Id on success.
func AddPROVEEDORES(m *PROVEEDORES) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetPROVEEDORESById retrieves PROVEEDORES by Id. Returns error if
// Id doesn't exist
func GetPROVEEDORESById(id int) (v *PROVEEDORES, err error) {
	o := orm.NewOrm()
	v = &PROVEEDORES{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllPROVEEDORES retrieves all PROVEEDORES matches certain condition. Returns empty list if
// no records exist
func GetAllPROVEEDORES(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(PROVEEDORES))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []PROVEEDORES
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdatePROVEEDORES updates PROVEEDORES by Id and returns error if
// the record to be updated doesn't exist
func UpdatePROVEEDORESById(m *PROVEEDORES) (err error) {
	o := orm.NewOrm()
	v := PROVEEDORES{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeletePROVEEDORES deletes PROVEEDORES by Id and returns error if
// the record to be deleted doesn't exist
func DeletePROVEEDORES(id int) (err error) {
	o := orm.NewOrm()
	v := PROVEEDORES{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&PROVEEDORES{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
