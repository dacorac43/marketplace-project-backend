package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/astaxie/beego/orm"
)

type CATEGORIAS struct {
	Id              int    `orm:"column(ID_CATEGORIA);auto"`
	TITULOCATEGORIA string `orm:"column(TITULO_CATEGORIA);size(1024)"`
}

func (t *CATEGORIAS) TableName() string {
	return "CATEGORIAS"
}

func init() {
	orm.RegisterModel(new(CATEGORIAS))
}

// AddCATEGORIAS insert a new CATEGORIAS into database and returns
// last inserted Id on success.
func AddCATEGORIAS(m *CATEGORIAS) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

type ErrorIntegridadCategoria string

func (e ErrorIntegridadCategoria) Error() string {
	return string(e)
}

// RevisarIntegridadCategoria revisa la integridad de la información
// registro de la categoria
func RevisarIntegridadCategorias(categoria CATEGORIAS) error {

	// El valor del título no debe encontrarse en la base de datos
	o := orm.NewOrm()
	qs := o.QueryTable(new(CATEGORIAS))

	// Obtener todos los valores de los titulos
	var listaTitulos orm.ParamsList
	qs.ValuesFlat(&listaTitulos, "TITULO_CATEGORIA")
	for _, titulo := range listaTitulos {
		if strings.EqualFold(titulo.(string), categoria.TITULOCATEGORIA) {
			return ErrorIntegridadCategoria("Ya existe un título similar en la base de datos")
		}
	}

	// Retornar nil si el título de la categoría es nuevo
	return nil
}

// GetCATEGORIASById retrieves CATEGORIAS by Id. Returns error if
// Id doesn't exist
func GetCATEGORIASById(id int) (v *CATEGORIAS, err error) {
	o := orm.NewOrm()
	v = &CATEGORIAS{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllCATEGORIAS retrieves all CATEGORIAS matches certain condition. Returns empty list if
// no records exist
func GetAllCATEGORIAS(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(CATEGORIAS))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []CATEGORIAS
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateCATEGORIAS updates CATEGORIAS by Id and returns error if
// the record to be updated doesn't exist
func UpdateCATEGORIASById(m *CATEGORIAS) (err error) {
	o := orm.NewOrm()
	v := CATEGORIAS{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteCATEGORIAS deletes CATEGORIAS by Id and returns error if
// the record to be deleted doesn't exist
func DeleteCATEGORIAS(id int) (err error) {
	o := orm.NewOrm()
	v := CATEGORIAS{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&CATEGORIAS{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
