package models

import (
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"

	"github.com/astaxie/beego/orm"
)

type USUARIOS struct {
	Id           int    `orm:"column(ID_USUARIO);auto"`
	NOMBRES      string `orm:"column(NOMBRES);size(1024)"`
	APELLIDOS    string `orm:"column(APELLIDOS);size(1024)"`
	CORREO       string `orm:"column(CORREO);size(128);unique"`
	HASHPASSWORD string `orm:"column(HASH_PASSWORD);size(1024)"`
	APITOKEN     string `orm:"column(API_TOKEN);size(1024);null"`
}

func (t *USUARIOS) TableName() string {
	return "USUARIOS"
}

func init() {
	orm.RegisterModel(new(USUARIOS))
}

// ErrorIntegridadUsuario es un error que sucede cuando la información del usuario no cumple
// los requisitos esperados
type ErrorIntegridadUsuario string

func (e ErrorIntegridadUsuario) Error() string {
	return string(e)
}

// RevisarIntegridadUsuario revisa la integridad de la información del usuario.
// esHash es una bandera que indica si el campo usuario.HASHPASSWORD es un hash o la contraseña en texto plano.
// revisarCorreo es una bandera que indica si se debe o no revisar la estructura y no repetición del correo.
func RevisarIntegridadUsuario(usuario USUARIOS, esHash bool, revisarCorreo bool) error {

	o := orm.NewOrm()

	// Revisar correo solo si se pide hacerlo
	if revisarCorreo {
		// Revisar que el correo tenga una estructura válida
		longitudCorreo := len(usuario.CORREO)
		if longitudCorreo < 5 || longitudCorreo > 128 || strings.Count(usuario.CORREO, "@") != 1 || strings.HasPrefix(usuario.CORREO, "@") ||
			strings.HasSuffix(usuario.CORREO, "@") {
			return ErrorIntegridadUsuario("Correo no tiene estructura válida")
		}

		// Revisar que el correo no está registrado
		if o.QueryTable(new(USUARIOS)).Filter("CORREO", usuario.CORREO).Exist() {
			return ErrorIntegridadUsuario("Correo ya registrado")
		}
	}

	// Revisar que los nombres, apellidos y contraseña tengan una longitud adecuada
	if longitudNombre := len(usuario.NOMBRES); longitudNombre < 5 || longitudNombre > 100 {
		return ErrorIntegridadUsuario("Longitud del nombre debe ser mayor que 4 y menor que 101")
	}
	if longitudApellido := len(usuario.APELLIDOS); longitudApellido < 5 || longitudApellido > 100 {
		return ErrorIntegridadUsuario("Longitud del apellido debe ser mayor que 4 y menor que 101")
	}

	// Si el campo usuario.HASHPASSWORD no es un hash sino la contraseña en texto plano
	if !esHash {
		if longitudPass := len(usuario.HASHPASSWORD); longitudPass < 8 || longitudPass > 100 {
			return ErrorIntegridadUsuario("Longitud de la contraseña debe ser mayor que 7 y menor que 101")
		}
		// Revisar contenido de la contraseña (al menos un número, una letra minúscula y una mayúscula)
		tieneNumero, _ := regexp.MatchString("[0-9]", usuario.HASHPASSWORD)
		tieneMinuscula, _ := regexp.MatchString("[a-z]", usuario.HASHPASSWORD)
		tieneMayuscula, _ := regexp.MatchString("[A-Z]", usuario.HASHPASSWORD)
		if !tieneNumero || !tieneMinuscula || !tieneMayuscula {
			return ErrorIntegridadUsuario("La contraseña debe tener al menos número, una letra minúscula y una letra mayúscula")
		}
	}

	// Retornar nil si no hubo error
	return nil
}

// AddUSUARIOS insert a new USUARIOS into database and returns
// last inserted Id on success.
func AddUSUARIOS(m *USUARIOS) (id int64, err error) {
	o := orm.NewOrm()
	// Calcular hash de la contraseña
	m.HASHPASSWORD = CalcularHashPassword(m.HASHPASSWORD)
	id, err = o.Insert(m)
	if err == nil {
		o.Raw("UPDATE USUARIOS SET API_TOKEN = NULL WHERE ID_USUARIO = ?", m.Id).Exec()
	}
	return
}

// GetUSUARIOSById retrieves USUARIOS by Id. Returns error if
// Id doesn't exist
func GetUSUARIOSById(id int) (v *USUARIOS, err error) {
	o := orm.NewOrm()
	v = &USUARIOS{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllUSUARIOS retrieves all USUARIOS matches certain condition. Returns empty list if
// no records exist
func GetAllUSUARIOS(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(USUARIOS))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		if strings.Contains(k, "isnull") {
			qs = qs.Filter(k, (v == "true" || v == "1"))
		} else {
			qs = qs.Filter(k, v)
		}
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []USUARIOS
	qs = qs.OrderBy(sortFields...)
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateUSUARIOS updates USUARIOS by Id and returns error if
// the record to be updated doesn't exist.
// esHash es una bandera que indica si m.HASHPASSWORD es un hash o la contraseña en texto plano
func UpdateUSUARIOSById(m *USUARIOS, esHash bool) (err error) {
	o := orm.NewOrm()
	v := USUARIOS{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {

		if !esHash {
			// Calcular hash de la contraseña, si esta está en texto plano
			m.HASHPASSWORD = CalcularHashPassword(m.HASHPASSWORD)
		}

		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteUSUARIOS deletes USUARIOS by Id and returns error if
// the record to be deleted doesn't exist
func DeleteUSUARIOS(id int) (err error) {
	o := orm.NewOrm()
	v := USUARIOS{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&USUARIOS{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
