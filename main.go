package main

import (
	"backendmarketplace/models"
	_ "backendmarketplace/routers"
	"fmt"

	"os"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/plugins/cors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/romanyx/polluter"
)

func init() {
	coneccion := beego.AppConfig.String("mysqluser") + ":" + beego.AppConfig.String("mysqlpass") + "@tcp(127.0.0.1:3306)/" + beego.AppConfig.String("mysqldb")
	orm.RegisterDataBase("default", "mysql", coneccion)
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"X-ACCESS_TOKEN", "Access-Control-Allow-Origin", "Authorization", "Origin", "x-requested-with", "Content-Type", "Content-Range", "Content-Disposition", "Content-Description", "Access-Control-Allow-Headers"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))
}

func llevarBaseDeDatosAEstadoInicialModoTest() {
	o := orm.NewOrm()
	// Eliminar registros
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
	// Insertar el administrador por defecto: correo -> admin@correo.co y pass -> 123
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin', 'admin', 'admin@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()
}

func llevarBaseDeDatosAEstadoInicialModoDev() {
	o := orm.NewOrm()
	// Eliminar registros
	o.Raw("DELETE FROM SERVICIOS").Exec()
	o.Raw("DELETE FROM PAQUETES").Exec()
	o.Raw("DELETE FROM CATEGORIAS").Exec()
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
	seed, errArchivoYaml := os.Open("otros/semillaBasedeDatos.yaml")
	if errArchivoYaml != nil {
		fmt.Printf("Fallo al abrir el archivo yaml: %s", errArchivoYaml)
	}
	defer seed.Close()
	db, errDB := orm.GetDB("default")
	if errDB != nil {
		fmt.Printf("Fallo al obtener base de datos: %s", errDB)
	}
	p := polluter.New(polluter.MySQLEngine(db))
	if errSeeder := p.Pollute(seed); errSeeder != nil {
		fmt.Printf("Fallo al realizar el seeding de la base de datos: %s", errSeeder)
	}

}

func main() {
	if beego.BConfig.RunMode == "dev" || beego.BConfig.RunMode == "test" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
		// Preparar estado inicial de la base de datos para usar la interfaz web en modo prueba
		if beego.BConfig.RunMode == "test" {
			llevarBaseDeDatosAEstadoInicialModoTest()
		} else if beego.BConfig.RunMode == "dev" {
			llevarBaseDeDatosAEstadoInicialModoDev()
		}
	}
	beego.Run()
}
