package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["backendmarketplace/controllers:ADMINISTRADORESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:ADMINISTRADORESController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:ADMINISTRADORESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:ADMINISTRADORESController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:ADMINISTRADORESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:ADMINISTRADORESController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:ADMINISTRADORESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:ADMINISTRADORESController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:ADMINISTRADORESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:ADMINISTRADORESController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:AUTENTICACIONController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:AUTENTICACIONController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:AUTENTICACIONController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:AUTENTICACIONController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CATEGORIASController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CATEGORIASController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CATEGORIASController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CATEGORIASController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CATEGORIASController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CATEGORIASController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CLIENTESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CLIENTESController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CLIENTESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CLIENTESController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CLIENTESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CLIENTESController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CLIENTESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CLIENTESController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CLIENTESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CLIENTESController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:COMENTARIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:COMENTARIOSController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:COMENTARIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:COMENTARIOSController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:COMENTARIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:COMENTARIOSController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:COMENTARIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:COMENTARIOSController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:COMENTARIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:COMENTARIOSController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CONTENIDOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CONTENIDOSController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CONTENIDOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CONTENIDOSController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CONTENIDOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CONTENIDOSController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CONTENIDOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CONTENIDOSController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:CONTENIDOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:CONTENIDOSController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:FOROSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:FOROSController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:FOROSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:FOROSController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:FOROSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:FOROSController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:FOROSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:FOROSController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:FOROSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:FOROSController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PAQUETESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PAQUETESController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PAQUETESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PAQUETESController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PAQUETESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PAQUETESController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PAQUETESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PAQUETESController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PAQUETESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PAQUETESController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PERMISOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PERMISOSController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PERMISOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PERMISOSController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PERMISOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PERMISOSController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PERMISOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PERMISOSController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PERMISOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PERMISOSController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PROVEEDORESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PROVEEDORESController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PROVEEDORESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PROVEEDORESController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PROVEEDORESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PROVEEDORESController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PROVEEDORESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PROVEEDORESController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PROVEEDORESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PROVEEDORESController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PUBLICACIONESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PUBLICACIONESController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PUBLICACIONESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PUBLICACIONESController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PUBLICACIONESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PUBLICACIONESController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PUBLICACIONESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PUBLICACIONESController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:PUBLICACIONESController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:PUBLICACIONESController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:SERVICIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:SERVICIOSController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:SERVICIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:SERVICIOSController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:SERVICIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:SERVICIOSController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:SERVICIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:SERVICIOSController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:SERVICIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:SERVICIOSController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:USUARIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:USUARIOSController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:USUARIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:USUARIOSController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:USUARIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:USUARIOSController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:USUARIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:USUARIOSController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["backendmarketplace/controllers:USUARIOSController"] = append(beego.GlobalControllerRouter["backendmarketplace/controllers:USUARIOSController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
