// @APIVersion 1.0.0
// @Title beego Test API
// @Description Beego is cool, but the doc could be far clearer.
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"backendmarketplace/controllers"

	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1",

		beego.NSNamespace("/TOKEN",
			beego.NSInclude(
				&controllers.AUTENTICACIONController{},
			),
		),

		beego.NSNamespace("/ADMINISTRADORES",
			beego.NSInclude(
				&controllers.ADMINISTRADORESController{},
			),
		),

		beego.NSNamespace("/CATEGORIAS",
			beego.NSInclude(
				&controllers.CATEGORIASController{},
			),
		),

		beego.NSNamespace("/CLIENTES",
			beego.NSInclude(
				&controllers.CLIENTESController{},
			),
		),
		/*
			beego.NSNamespace("/COMENTARIOS",
				beego.NSInclude(
					&controllers.COMENTARIOSController{},
				),
			),

			beego.NSNamespace("/CONTENIDOS",
				beego.NSInclude(
					&controllers.CONTENIDOSController{},
				),
			),

			beego.NSNamespace("/FOROS",
				beego.NSInclude(
					&controllers.FOROSController{},
				),
			),
		*/
		beego.NSNamespace("/PAQUETES",
			beego.NSInclude(
				&controllers.PAQUETESController{},
			),
		),
		/*
			beego.NSNamespace("/PERMISOS",
				beego.NSInclude(
					&controllers.PERMISOSController{},
				),
			),
		*/
		beego.NSNamespace("/PROVEEDORES",
			beego.NSInclude(
				&controllers.PROVEEDORESController{},
			),
		),
		/*
			beego.NSNamespace("/PUBLICACIONES",
				beego.NSInclude(
					&controllers.PUBLICACIONESController{},
				),
			),

			beego.NSNamespace("/SERVICIOS",
				beego.NSInclude(
					&controllers.SERVICIOSController{},
				),
			),

			beego.NSNamespace("/USUARIOS",
				beego.NSInclude(
					&controllers.USUARIOSController{},
				),
			),*/
	)
	beego.AddNamespace(ns)
}
