package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CrearCamposCiudadDestinoYCuposPublicadosEnPaquetes_20190304_182613 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CrearCamposCiudadDestinoYCuposPublicadosEnPaquetes_20190304_182613{}
	m.Created = "20190304_182613"

	migration.Register("CrearCamposCiudadDestinoYCuposPublicadosEnPaquetes_20190304_182613", m)
}

// Run the migrations
func (m *CrearCamposCiudadDestinoYCuposPublicadosEnPaquetes_20190304_182613) Up() {
	m.SQL("alter table PAQUETES add column CIUDAD_DESTINO varchar(1024)")
	m.SQL("alter table PAQUETES add column CUPOS_PUBLICADOS int NOT NULL")
}

// Reverse the migrations
func (m *CrearCamposCiudadDestinoYCuposPublicadosEnPaquetes_20190304_182613) Down() {
	// Se quita las columnas creadas
	m.SQL("alter table PAQUETES drop column CIUDAD_DESTINO")
	m.SQL("alter table PAQUETES drop column CUPOS_PUBLICADOS")
}
