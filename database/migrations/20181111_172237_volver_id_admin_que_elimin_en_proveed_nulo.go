package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type VolverIdAdminQueEliminEnProveedNulo_20181111_172237 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &VolverIdAdminQueEliminEnProveedNulo_20181111_172237{}
	m.Created = "20181111_172237"

	migration.Register("VolverIdAdminQueEliminEnProveedNulo_20181111_172237", m)
}

// Run the migrations
func (m *VolverIdAdminQueEliminEnProveedNulo_20181111_172237) Up() {
	m.SQL("alter table PROVEEDORES modify ID_ADMIN_ELIMINACION int NULL")

}

// Reverse the migrations
func (m *VolverIdAdminQueEliminEnProveedNulo_20181111_172237) Down() {
	// Se quita la restricción de llave foránea momentánemente
	m.SQL("alter table PROVEEDORES drop foreign key FK_ADMINISTRADOR_QUE_ELIMINO_PROVEEDOR")
	m.SQL("alter table PROVEEDORES modify ID_ADMIN_ELIMINACION int NOT NULL")
	// Se vuelve a colocar la restricción
	m.SQL("alter table PROVEEDORES add constraint FK_ADMINISTRADOR_QUE_ELIMINO_PROVEEDOR foreign key (ID_ADMIN_ELIMINACION) references ADMINISTRADORES (ID_ADMIN) on delete restrict on update restrict")

}
