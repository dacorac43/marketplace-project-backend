package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type VolverCorreoUnicoUsuarios_20181109_190035 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &VolverCorreoUnicoUsuarios_20181109_190035{}
	m.Created = "20181109_190035"

	migration.Register("VolverCorreoUnicoUsuarios_20181109_190035", m)
}

// Run the migrations
func (m *VolverCorreoUnicoUsuarios_20181109_190035) Up() {
	m.SQL("alter table USUARIOS modify column CORREO varchar(128)")
	m.SQL("alter table USUARIOS add constraint CORREO_ES_NOMBRE_DE_USUARIO unique (CORREO)")
}

// Reverse the migrations
func (m *VolverCorreoUnicoUsuarios_20181109_190035) Down() {
	m.SQL("alter table USUARIOS drop index CORREO_ES_NOMBRE_DE_USUARIO")
	m.SQL("alter table USUARIOS modify column CORREO varchar(1024)")
}
