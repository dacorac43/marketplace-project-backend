package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type VolverIdAdminValidEnClienteNulo_20181111_121810 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &VolverIdAdminValidEnClienteNulo_20181111_121810{}
	m.Created = "20181111_121810"

	migration.Register("VolverIdAdminValidEnClienteNulo_20181111_121810", m)
}

// Run the migrations
func (m *VolverIdAdminValidEnClienteNulo_20181111_121810) Up() {
	m.SQL("alter table CLIENTES modify ID_ADMIN_VALIDACION int NULL")

}

// Reverse the migrations
func (m *VolverIdAdminValidEnClienteNulo_20181111_121810) Down() {

	// Se quita la restricción de llave foránea momentánemente
	m.SQL("alter table CLIENTES drop foreign key FK_ADMINISTRADOR_VALIDO_REGISTRO_CLIENTE")
	m.SQL("alter table CLIENTES modify ID_ADMIN_VALIDACION int NOT NULL")
	// Se vuelve a colocar la restricción
	m.SQL("alter table CLIENTES add constraint FK_ADMINISTRADOR_VALIDO_REGISTRO_CLIENTE foreign key (ID_ADMIN_VALIDACION) references ADMINISTRADORES (ID_ADMIN) on delete restrict on update restrict")
}
