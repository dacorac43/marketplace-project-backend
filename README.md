# Backend del Marketplace

## Instalación del ambiente

### 1. Instalar el lenguaje de programación Go:
Ir a [la siguiente dirección](https://golang.org/doc/install) para descargarlo (mínimo la versión 1.1) y seguir las instrucciones. No olvidarse de configurar las variables de ambiente PATH, según diga las instrucciones, y GOPATH ([enlace que explica qué es esta última](https://github.com/golang/go/wiki/SettingGOPATH)). También incluir la ruta GOPATH/bin en la variable PATH.

### 2. Instalar Beego:
Estos comandos se pueden ejecutar en cualquier ubicación:
```bash
go get github.com/astaxie/beego
go get github.com/beego/bee
```

### 3. Instalar driver para MySQL:
Este comando también se puede ejecutar en cualquier ubicación:
```bash
go get github.com/go-sql-driver/mysql
```

## Configuración del ambiente:

El repositorio **debe** clonarse en la ruta GOPATH/src, es decir, se **debe** hacer *git clone* estando en esa ubicación. De aquí en adelante se asume que el repositorio ya está clonado.

### 1. Cargar base de datos inicial en el servidor local de MySQL:

Para esto se debe ejecutar el código sql presente en el archivo de este repositorio cuya ruta es **otros/baseDeDatosInicial.sql**.

### 2. Crear archivo de configuración local de la base de datos:

Se debe crear un archivo en la carpeta **conf** llamado **conexbasedatos.conf**. Copiar el contenido del archivo **conf/conexbasedatos-ejemplo.conf** a este nuevo archivo y ajustarlo según cómo y dónde se haya cargado la base de datos inicial.

### 3. Ejecutar migraciones existentes de la base de datos:

Iniciar el servidor MySQL. Ejecutar el siguiente comando dentro de la carpeta del repositorio:
```bash
bee migrate -conn="user:password@tcp(127.0.0.1:3306)/database"
```
Donde *user*, *password* y *database* se remplazan por los valores usados en la configuración local (es necesario escribirlos en el comando, ya que para esta operación no lee el archivo de configuración local).

**Nota:** Si muestra un error indicando que no existe la carpeta **database/migrations** es porque el código a ese momento no cuenta con migraciones (al momento de escribir esto no las hay).

## Prueba del ambiente

+ Iniciar el servidor MySQL.

+ Ejecutar el siguiente comando en la carpeta del repositorio: 
```bash 
bee run -downdoc=true -gendoc=true
```

+ Visitar la dirección <localhost:8080/swagger>. Esta es una interfaz web que documenta la API hasta el momento. 

+ La interfaz permite hacer consultas. Probar GET /USUARIOS/ y asegurarse que la respuesta tiene código 200.

## Para ejecución de pruebas unitarias

+ Instalar el siguiente paquete:
```bash
go get github.com/go-ini/ini
```
**Nota:** se puede ejecutar en cualquier ubicación.

+ Iniciar el servidor MySQL.

+ Iniciar backend con el siguiente comando (en la carpeta del repositorio):
```bash 
bee run -runmode=test
```

+ Para ejecutar las pruebas unitarias se tienen varias opciones (siempre ejecutar estos comandos en la carpeta del repositorio):

1. Ejecutar todas las pruebas unitarias sin usar el caché de resultados (para eso sirve el *-count=1*):
```bash 
go test -v backendmarketplace/tests -count=1
```
2. Ejecutar únicamente la subprueba que está en la función especificada y sin caché:
```bash
go test -v backendmarketplace/tests -run [NombreFunción]/[NombreSubprueba] -count=1
```
3. Ejecutar únicamente la función especificada y sin caché:
```bash
go test -v backendmarketplace/tests -run [NombreFunción] -count=1
```
4. Ejecutar todas las pruebas unitarias relacionadas con un recurso (usar el nombre del archivo del controlador correspondiente, sin el .go) y sin caché:
```bash
go test -v backendmarketplace/tests -run [NombreRecurso] -count=1
```
## Darle un estado inicial a la base de datos de desarrollo

+ Instalar el siguiente paquete:
```bash
go get github.com/romanyx/polluter
```
+ Asegurarse de que en el archivo de la carpeta **conf** llamado **conexbasedatos.conf** exista una base de datos para el ambiente **dev** (esa será la base de datos que adquirirá el estado inicial). Ver archivo **conexbasedatos-ejemplo.conf** para revisar la sintaxis y el archivo en la carpeta **otros** llamado **semillaBasedeDatos.yaml** para ver el valor de cada registro.

+ Iniciar el servidor MySQL.

+ Iniciar el backend con el siguiente comando (en la carpeta del repositorio):
```bash 
bee run
```