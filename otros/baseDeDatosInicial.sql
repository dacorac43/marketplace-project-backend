/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     07/11/2018 19:50:28                          */
/*==============================================================*/


drop table if exists ADMINISTRADORES;

drop table if exists ADMINISTRADORESFOROS;

drop table if exists CATEGORIAS;

drop table if exists CLIENTES;

drop table if exists COMENTARIOS;

drop table if exists CONTENIDOS;

drop table if exists CONTENIDOSPAQUETES;

drop table if exists FOROS;

drop table if exists MENSAJESENTREUSUARIOS;

drop table if exists PAQUETES;

drop table if exists PAQUETESBUSCADOSPORCLIENTES;

drop table if exists PAQUETESCOMPRADOSPORCLIENTES;

drop table if exists PERMISOS;

drop table if exists PERMISOSUSUARIOS;

drop table if exists PROVEEDORES;

drop table if exists PUBLICACIONES;

drop table if exists SERVICIOS;

drop table if exists USUARIOS;

/*==============================================================*/
/* Table: ADMINISTRADORES                                       */
/*==============================================================*/
create table ADMINISTRADORES
(
   ID_ADMIN             int not null,
   primary key (ID_ADMIN)
);

/*==============================================================*/
/* Table: ADMINISTRADORESFOROS                                  */
/*==============================================================*/
create table ADMINISTRADORESFOROS
(
   ID_FORO              int not null,
   ID_ADMIN             int not null,
   primary key (ID_FORO, ID_ADMIN)
);

/*==============================================================*/
/* Table: CATEGORIAS                                            */
/*==============================================================*/
create table CATEGORIAS
(
   ID_CATEGORIA         int not null auto_increment,
   TITULO_CATEGORIA     varchar(1024) not null,
   primary key (ID_CATEGORIA)
);

/*==============================================================*/
/* Table: CLIENTES                                              */
/*==============================================================*/
create table CLIENTES
(
   ID_USUARIO           int not null,
   ID_ADMIN_VALIDACION  int not null,
   primary key (ID_USUARIO)
);

/*==============================================================*/
/* Table: COMENTARIOS                                           */
/*==============================================================*/
create table COMENTARIOS
(
   ID_COMENTARIO        int not null auto_increment,
   ID_FORO              int not null,
   FECHA_PUBLICACION    time not null,
   CONTENIDO            varchar(1024) not null,
   primary key (ID_COMENTARIO)
);

/*==============================================================*/
/* Table: CONTENIDOS                                            */
/*==============================================================*/
create table CONTENIDOS
(
   ID_CONTENIDO         int not null auto_increment,
   ID_PUBLICACION       int not null,
   TIPO                 varchar(1024) not null,
   DESCRIPCION_CONTENIDO varchar(1024) not null,
   primary key (ID_CONTENIDO)
);

/*==============================================================*/
/* Table: CONTENIDOSPAQUETES                                    */
/*==============================================================*/
create table CONTENIDOSPAQUETES
(
   ID_CONTENIDO         int not null,
   ID_PAQUETE           int not null,
   primary key (ID_CONTENIDO, ID_PAQUETE)
);

/*==============================================================*/
/* Table: FOROS                                                 */
/*==============================================================*/
create table FOROS
(
   ID_FORO              int not null auto_increment,
   TITULO_FORO          varchar(1024) not null,
   primary key (ID_FORO)
);

/*==============================================================*/
/* Table: MENSAJESENTREUSUARIOS                                 */
/*==============================================================*/
create table MENSAJESENTREUSUARIOS
(
   ID_REMITENTE         int not null,
   ID_DESTINATARIO      int not null,
   MENSAJE              varchar(1024) not null,
   primary key (ID_REMITENTE, ID_DESTINATARIO)
);

/*==============================================================*/
/* Table: PAQUETES                                              */
/*==============================================================*/
create table PAQUETES
(
   ID_PAQUETE           int not null auto_increment,
   ID_PROVEEDOR         int not null,
   DISPONIBILIDAD       int not null,
   COSTO                float not null,
   PROMEDIO             float,
   NUM_CALIFICACIONES   int not null,
   DESCUENTO            float,
   TITULO_PAQUETE       varchar(1024) not null,
   primary key (ID_PAQUETE)
);

/*==============================================================*/
/* Table: PAQUETESBUSCADOSPORCLIENTES                           */
/*==============================================================*/
create table PAQUETESBUSCADOSPORCLIENTES
(
   ID_PAQUETE           int not null,
   ID_USUARIO           int not null,
   primary key (ID_PAQUETE, ID_USUARIO)
);

/*==============================================================*/
/* Table: PAQUETESCOMPRADOSPORCLIENTES                          */
/*==============================================================*/
create table PAQUETESCOMPRADOSPORCLIENTES
(
   ID_PAQUETE           int not null,
   ID_USUARIO           int not null,
   primary key (ID_PAQUETE, ID_USUARIO)
);

/*==============================================================*/
/* Table: PERMISOS                                              */
/*==============================================================*/
create table PERMISOS
(
   ID_PERMISO           int not null auto_increment,
   DESCRIPCION_PERMISO  varchar(1024) not null,
   primary key (ID_PERMISO)
);

/*==============================================================*/
/* Table: PERMISOSUSUARIOS                                      */
/*==============================================================*/
create table PERMISOSUSUARIOS
(
   ID_USUARIO           int not null,
   ID_PERMISO           int not null,
   primary key (ID_USUARIO, ID_PERMISO)
);

/*==============================================================*/
/* Table: PROVEEDORES                                           */
/*==============================================================*/
create table PROVEEDORES
(
   ID_PROVEEDOR         int not null,
   ID_ADMIN_REGISTRO    int not null,
   ID_ADMIN_ELIMINACION int,
   ORGANIZACION         varchar(1024),
   primary key (ID_PROVEEDOR)
);

/*==============================================================*/
/* Table: PUBLICACIONES                                         */
/*==============================================================*/
create table PUBLICACIONES
(
   ID_PUBLICACION       int not null auto_increment,
   ID_AUTOR_PROVEEDOR   int,
   ID_AUTOR_ADMINISTRADOR int,
   TITULO_PUBLICACION   varchar(1024) not null,
   primary key (ID_PUBLICACION)
);

/*==============================================================*/
/* Table: SERVICIOS                                             */
/*==============================================================*/
create table SERVICIOS
(
   ID_SERVICIO          int not null auto_increment,
   ID_PAQUETE           int not null,
   ID_CATEGORIA         int not null,
   TITULO_SERVICIO      varchar(1024) not null,
   primary key (ID_SERVICIO)
);

/*==============================================================*/
/* Table: USUARIOS                                              */
/*==============================================================*/
create table USUARIOS
(
   ID_USUARIO           int not null auto_increment,
   NOMBRES              varchar(1024) not null,
   APELLIDOS            varchar(1024) not null,
   CORREO               varchar(1024) not null,
   HASH_PASSWORD        varchar(1024) not null,
   API_TOKEN            varchar(1024),
   primary key (ID_USUARIO)
);

alter table ADMINISTRADORES add constraint FK_ADMINISTRADOR_ES_UN_USUARIO foreign key (ID_ADMIN)
      references USUARIOS (ID_USUARIO) on delete restrict on update restrict;

alter table ADMINISTRADORESFOROS add constraint FK_ADMINISTRADOR_QUE_GESTIONA_FORO foreign key (ID_ADMIN)
      references ADMINISTRADORES (ID_ADMIN) on delete restrict on update restrict;

alter table ADMINISTRADORESFOROS add constraint FK_FORO_GESTIONADO_POR_ADMINISTRADOR foreign key (ID_FORO)
      references FOROS (ID_FORO) on delete restrict on update restrict;

alter table CLIENTES add constraint FK_ADMINISTRADOR_VALIDO_REGISTRO_CLIENTE foreign key (ID_ADMIN_VALIDACION)
      references ADMINISTRADORES (ID_ADMIN) on delete restrict on update restrict;

alter table CLIENTES add constraint FK_CLIENTE_ES_UN_USUARIO foreign key (ID_USUARIO)
      references USUARIOS (ID_USUARIO) on delete restrict on update restrict;

alter table COMENTARIOS add constraint FK_FORO_ALMACENA_COMENTARIOS foreign key (ID_FORO)
      references FOROS (ID_FORO) on delete restrict on update restrict;

alter table CONTENIDOS add constraint FK_PUBLICACION_PRESENTA_CONTENIDO foreign key (ID_PUBLICACION)
      references PUBLICACIONES (ID_PUBLICACION) on delete restrict on update restrict;

alter table CONTENIDOSPAQUETES add constraint FK_CONTENIDO_APARECE_EN_PAQUETE foreign key (ID_CONTENIDO)
      references CONTENIDOS (ID_CONTENIDO) on delete restrict on update restrict;

alter table CONTENIDOSPAQUETES add constraint FK_PAQUETE_TIENE_CONTENIDO foreign key (ID_PAQUETE)
      references PAQUETES (ID_PAQUETE) on delete restrict on update restrict;

alter table MENSAJESENTREUSUARIOS add constraint FK_USUARIO_ES_DESTINATARIO_DE_UN_MENSAJE foreign key (ID_DESTINATARIO)
      references USUARIOS (ID_USUARIO) on delete restrict on update restrict;

alter table MENSAJESENTREUSUARIOS add constraint FK_USUARIO_ES_REMITENTE_DE_UN_MENSAJE foreign key (ID_REMITENTE)
      references USUARIOS (ID_USUARIO) on delete restrict on update restrict;

alter table PAQUETES add constraint FK_PROVEEDOR_OFRECE_PAQUETE foreign key (ID_PROVEEDOR)
      references PROVEEDORES (ID_PROVEEDOR) on delete restrict on update restrict;

alter table PAQUETESBUSCADOSPORCLIENTES add constraint FK_CLIENTE_BUSCO_PAQUETE foreign key (ID_USUARIO)
      references CLIENTES (ID_USUARIO) on delete restrict on update restrict;

alter table PAQUETESBUSCADOSPORCLIENTES add constraint FK_PAQUETE_FUE_BUSCADO_POR_CLIENTE foreign key (ID_PAQUETE)
      references PAQUETES (ID_PAQUETE) on delete restrict on update restrict;

alter table PAQUETESCOMPRADOSPORCLIENTES add constraint FK_CLIENTE_COMPRO_PAQUETE foreign key (ID_USUARIO)
      references CLIENTES (ID_USUARIO) on delete restrict on update restrict;

alter table PAQUETESCOMPRADOSPORCLIENTES add constraint FK_PAQUETE_FUE_COMPRADO_POR_CLIENTE foreign key (ID_PAQUETE)
      references PAQUETES (ID_PAQUETE) on delete restrict on update restrict;

alter table PERMISOSUSUARIOS add constraint FK_PERMISO_DEFINIDO_PARA_USUARIO foreign key (ID_PERMISO)
      references PERMISOS (ID_PERMISO) on delete restrict on update restrict;

alter table PERMISOSUSUARIOS add constraint FK_USUARIO_QUE_POSEE_PERMISO foreign key (ID_USUARIO)
      references USUARIOS (ID_USUARIO) on delete restrict on update restrict;

alter table PROVEEDORES add constraint FK_ADMINISTRADOR_QUE_ELIMINO_PROVEEDOR foreign key (ID_ADMIN_ELIMINACION)
      references ADMINISTRADORES (ID_ADMIN) on delete restrict on update restrict;

alter table PROVEEDORES add constraint FK_ADMINISTRADOR_QUE_REGISTRO_PROVEEDOR foreign key (ID_ADMIN_REGISTRO)
      references ADMINISTRADORES (ID_ADMIN) on delete restrict on update restrict;

alter table PROVEEDORES add constraint FK_PROVEEDOR_ES_UN_USUARIO foreign key (ID_PROVEEDOR)
      references USUARIOS (ID_USUARIO) on delete restrict on update restrict;

alter table PUBLICACIONES add constraint FK_ADMINISTRADOR_INFORMA_CON_PUBLICACION foreign key (ID_AUTOR_ADMINISTRADOR)
      references ADMINISTRADORES (ID_ADMIN) on delete restrict on update restrict;

alter table PUBLICACIONES add constraint FK_PROVEEDOR_REALIZA_PUBLICACION foreign key (ID_AUTOR_PROVEEDOR)
      references PROVEEDORES (ID_PROVEEDOR) on delete restrict on update restrict;

alter table SERVICIOS add constraint FK_PAQUETE_CONTIENE_SERVICIO foreign key (ID_PAQUETE)
      references PAQUETES (ID_PAQUETE) on delete restrict on update restrict;

alter table SERVICIOS add constraint FK_SERVICIO_TIENE_CATEGORIA foreign key (ID_CATEGORIA)
      references CATEGORIAS (ID_CATEGORIA) on delete restrict on update restrict;

