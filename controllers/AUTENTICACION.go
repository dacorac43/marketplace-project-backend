package controllers

import (
	"backendmarketplace/models"
	"encoding/json"
	"fmt"

	"github.com/astaxie/beego/orm"

	"github.com/astaxie/beego"
)

// AUTENTICACIONController operations for AUTENTICACION
type AUTENTICACIONController struct {
	beego.Controller
}

// BODY_POST_TOKEN Estructura para parsear el cuerpo de una solicitud POST para la creación de un TOKEN
type BODY_POST_TOKEN struct {
	Correo   string
	Password string
}

// BODY_RESPUESTA_POST_TOKEN Estructura que contiene la respuesta a una solicitud POST para la creación de un TOKEN
type BODY_RESPUESTA_POST_TOKEN struct {
	Token    string
	Rol      int
	Nombre   string
	Apellido string
	Correo   string
	Id       int
}

type BODY_DELETE_TOKEN = BODY_RESPUESTA_POST_TOKEN

// URLMapping ...
func (c *AUTENTICACIONController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Create
// @Description Crea y retorna un token si la autenticación es exitosa, además de los nombres, apellidos, correo, rol (1 para admin, 2 para proveedor y 3 para cliente) e id del registro en la base de datos
// @Param	body		body 	controllers.AUTENTICACION.BODY_POST_TOKEN	true		"Valores necesarios para realizar autenticación"
// @Success 201 {object} controllers.AUTENTICACION.BODY_RESPUESTA_POST_TOKEN
// @Failure 401 Autenticación fallida (el usuario no está registrado)
// @Failure 400 Error en la solicitud (hay un error en la estructura del JSON enviado)
// @router / [post]
func (c *AUTENTICACIONController) Post() {

	var v BODY_POST_TOKEN
	o := orm.NewOrm()
	var resultadoUsuario models.USUARIOS

	errorBody := json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	// Si el cuerpo de la solicitud es correcto
	if errorBody == nil {
		qs := o.QueryTable(new(models.USUARIOS))
		// Obtener registro con ese correo y hash de la contraseña
		errorNoRegistrado := qs.Filter("CORREO", v.Correo).
			Filter("HASH_PASSWORD", models.CalcularHashPassword(v.Password)).
			One(&resultadoUsuario)
		// Si el usuario está registrado
		if errorNoRegistrado == nil {

			// Generar token
			resultadoUsuario.APITOKEN = models.CalcularApiToken(resultadoUsuario)
			// Actualizar registro
			o.Update(&resultadoUsuario)
			// Generación exitosa del token
			c.Ctx.Output.SetStatus(201)
			// Determinar rol del usuario
			var rol int
			if o.QueryTable(new(models.ADMINISTRADORES)).Filter("ID_ADMIN", resultadoUsuario.Id).Exist() {
				rol = 1
			} else if o.QueryTable(new(models.PROVEEDORES)).Filter("ID_PROVEEDOR", resultadoUsuario).Exist() {
				rol = 2
			} else {
				rol = 3
			}
			// Retornar token
			c.Data["json"] = BODY_RESPUESTA_POST_TOKEN{Token: resultadoUsuario.APITOKEN, Rol: rol, Nombre: resultadoUsuario.NOMBRES,
				Apellido: resultadoUsuario.APELLIDOS, Correo: resultadoUsuario.CORREO, Id: resultadoUsuario.Id}
			c.ServeJSON()
			return
		}

		// Error en la autenticación
		c.Ctx.Output.SetStatus(401) // Unauthorized
		c.Data["json"] = "Autenticación fallida"
		c.ServeJSON()
		return

	}

	// Si el cuerpo de la solicitud es incorrecto
	c.Ctx.Output.SetStatus(400) // Bad Request
	c.Data["json"] = "Error en la solicitud"
	c.ServeJSON()

}

// Delete ...
// @Title Delete
// @Description Elimina token de acceso en la base de datos
// @Param 	body 	body 	controllers.AUTENTICACION.BODY_RESPUESTA_POST_TOKEN true "Token a eliminar"
// @Success 200 Token eliminado
// @Failure 400 La solicitud está mal hecha
// @Failure 403 Token no válido
// @router / [delete]
func (c *AUTENTICACIONController) Delete() {
	var v BODY_DELETE_TOKEN
	errorBody := json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	if errorBody == nil {
		resultadoUsuario := models.USUARIOS{APITOKEN: v.Token}
		o := orm.NewOrm()
		errorNoToken := o.Read(&resultadoUsuario, "API_TOKEN")
		if errorNoToken == nil {
			o.Raw("UPDATE USUARIOS SET API_TOKEN = NULL WHERE ID_USUARIO = ?", resultadoUsuario.Id).Exec()
			c.Ctx.Output.SetStatus(200)
		} else {
			// El token no existe
			c.Ctx.Output.SetStatus(403)
		}
	} else {
		// La solicitud está mal hecha
		fmt.Println(errorBody.Error())
		c.Ctx.Output.SetStatus(400)
	}
	c.ServeJSON()
}
