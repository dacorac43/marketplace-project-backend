package controllers

import (
	"backendmarketplace/models"
	"encoding/json"
	"errors"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

// PROVEEDORESController operations for PROVEEDORES
type PROVEEDORESController struct {
	beego.Controller
}

// Estructura para parsear el cuerpo de una solicitud POST para la creación de un PROVEEDOR
type BODY_POST_PROVEEDORES struct {
	Apellidos    string
	Correo       string
	Password     string
	Nombres      string
	Organizacion string
	Token        string
}

type BodyPutProveedores struct {
	Apellidos    string
	Correo       string
	Password     string
	Nombres      string
	Organizacion string
	Token        string
}

// BodyRespuestaGetOneProveedores Estructura que contiene la respuesta a una
// solicitud GET para la obtención de la información de un PROVEEDORES en particular
type BodyRespuestaGetOneProveedores struct {
	Nombres   string
	Apellidos string
	Correo    string
}

// BodyRespuestaGetOneProveedoresParaAll Estructura que contiene la respuesta de un
// solo registro que compone la respuesta para BodyRespuestaGetAllProveedores
type BodyRespuestaGetOneProveedoresParaAll struct {
	Nombres   string
	Apellidos string
	Correo    string
	Id        int
}

// BodyGetAllProveedores Estructura para parsear el cuerpo de una solicitud GET para la lectura de los proveedores
//type BodyGetAllProveedores struct {
//	TokenUsuario string
//}

// BodyGetOneProveedores Estructura para parsear el cuerpo de una solicitud GET para la lectura de un registro
// particular de un proveedor con el :id enviado
//type BodyGetOneProveedores = BodyGetAllProveedores

// BodyRespuestaGetAllProveedores Estructura que contiene la respuesta a una
// solicitud GET para la obtención de la información de todos los PROVEEDORES
type BodyRespuestaGetAllProveedores struct {
	Proveedores []BodyRespuestaGetOneProveedoresParaAll
}

// Estructura que contiene la respuesta a una solicitud POST para la creación de un CLIENTE
type BODY_RESPUESTA_POST_PROVEEDORES struct {
	Id int
}

// Estructura que contiene el cuerpo a una solicitud DELETE para la eliminación de un PROVEEDOR
type BodyDeleteProveedores struct {
	Token string
}

// URLMapping ...
func (c *PROVEEDORESController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Post
// @Description Crear un nuevo registro de proveedor
// @Param	body		body 	controllers.BODY_POST_PROVEEDORES	true		"Estructura del JSON para enviar en el cuerpo de la solicitud"
// @Success 201 {object} controllers.BODY_RESPUESTA_POST_PROVEEDORES
// @Failure 400 Error en los datos (retorna error explicativo) o estructura de la solicitud incorrecta
// @Failure 403 Token no válido
// @Failure 500 Error en el servidor
// @router / [post]
func (c *PROVEEDORESController) Post() {
	var v BODY_POST_PROVEEDORES
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {
		// Revisar token
		var usuarioAdministrador models.USUARIOS
		tokenAdmi := v.Token
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))
		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenAdmi).One(&usuarioAdministrador)

		// Si el token no está en la base de datos o no pertenece a un administrador
		if errorTokenNoEncontrado != nil || !o.QueryTable(new(models.ADMINISTRADORES)).Filter("ID_ADMIN", usuarioAdministrador.Id).Exist() {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.Data["json"] = "Token no válido"
			c.ServeJSON()
			return
		}

		// Crear objetos proveedor y usuario de acuerdo a la solicitud realizada
		var usuario models.USUARIOS
		var proveedor models.PROVEEDORES

		// Inicializar de forma correspondiente
		usuario.APELLIDOS = v.Apellidos
		usuario.CORREO = v.Correo
		usuario.NOMBRES = v.Nombres
		usuario.HASHPASSWORD = v.Password
		proveedor.ORGANIZACION = v.Organizacion
		proveedor.IDADMINREGISTRO = &models.ADMINISTRADORES{Id: usuarioAdministrador.Id}

		// Verificar integridad de la información enviada (solo la parte del usuario)
		if errorIntegridadInfoUsuario := models.RevisarIntegridadUsuario(usuario, false, true); errorIntegridadInfoUsuario != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(400)
			c.Data["json"] = errorIntegridadInfoUsuario.Error()
			c.ServeJSON()
			return
		} else if errorIntegridadInfoProv := models.RevisarIntegridadProveedor(proveedor); errorIntegridadInfoProv != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(400)
			c.Data["json"] = errorIntegridadInfoProv.Error()
			c.ServeJSON()
			return
		}

		// Insertar registro del usuario
		if id_usuario, error_insercion_usuario := models.AddUSUARIOS(&usuario); error_insercion_usuario == nil {
			// Asignar mismo id al proveedor
			proveedor.Id = int(id_usuario)
			// Isertar registro del proveedor
			if _, error_insercion_proveedor := models.AddPROVEEDORES(&proveedor); error_insercion_proveedor == nil {
				c.Ctx.Output.SetStatus(201)
				// Retornar cuerpo de la respuesta
				c.Data["json"] = BODY_RESPUESTA_POST_PROVEEDORES{Id: int(id_usuario)}
			} else {
				// Eliminar registro del usuario
				models.DeleteUSUARIOS(proveedor.Id)
				c.Ctx.Output.SetStatus(500)
				c.Data["json"] = "Error en el servidor"
			}
		} else {
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = "Error en el servidor"
		}
	} else {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Estructura de la solicitud incorrecta"
	}
	c.ServeJSON()
}

// GetOne ...
// @Title Get One
// @Description Obtener el registro de un proveedor usando su :id (usar el header "Authorization" así "Bearer Token")
// @Param	id		path 	string	true		"El :id del registro del proveedor cuya información se quiere obtener"
// @Success 200 {object} controllers.BodyRespuestaGetOneProveedores
// @Failure 400 El parámetro :id no es un entero o la estructura del cuerpo de la solicitud no es correcta
// @Failure 403 Token no está en la base de datos
// @Failure 404 No existe registro con ese :id
// @router /:id [get]
func (c *PROVEEDORESController) GetOne() {
	idStr := c.Ctx.Input.Param(":id")
	id, errID := strconv.Atoi(idStr)
	// Revisar si hubo un error al convertir el :id
	if errID != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "El parámetro :id no es un entero"
		c.ServeJSON()
		return
	}
	// Intentar convertir cuerpo de la solicitud
	token := strings.Split(c.Ctx.Input.Header("Authorization"), " ")[1]
	//var v BodyGetOneProveedores
	//longitudContenidoCuerpo, _ := strconv.Atoi(c.Ctx.Input.Header("Content-Length"))
	//cuerpoSolicitud := c.Ctx.Input.CopyBody(int64(longitudContenidoCuerpo))
	//errorConversionJSON := json.Unmarshal(cuerpoSolicitud, &v)

	//if errorConversionJSON == nil {

	//	token := v.TokenUsuario
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.USUARIOS))

	// Si el token no está en la base de datos
	if !qs.Filter("API_TOKEN", token).Exist() {
		// El solicitante no está autorizado para realizar la operación
		c.Ctx.Output.SetStatus(403)
		c.Data["json"] = "Token no está en la base de datos"
	} else {
		// Si el token es correcto
		v, errorIDNoExiste := models.GetPROVEEDORESById(id)
		if errorIDNoExiste != nil {
			// El id enviado no existe
			c.Ctx.Output.SetStatus(404)
			c.Data["json"] = "Recurso no existe"
		} else {
			// Lectura exitosa
			registroUsuario, _ := models.GetUSUARIOSById(v.Id)
			c.Ctx.Output.SetStatus(200)
			c.Data["json"] = BodyRespuestaGetOneProveedores{Nombres: registroUsuario.NOMBRES,
				Apellidos: registroUsuario.APELLIDOS,
				Correo:    registroUsuario.CORREO,
			}
		}
	}
	//else {
	// Error en la estructura del body de la solicitud enviada
	//	c.Ctx.Output.SetStatus(400)
	//	c.Data["json"] = "Error al procesar la estructura del cuerpo de la solicitud"
	//}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description Obtener todos los registros de proveedores en la base de datos (usar el header "Authorization" así "Bearer Token")
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} controllers.BodyRespuestaGetAllProveedores
// @Failure 400 Error en la estructura del body de la solicitud enviada o en la query string
// @Failure 403 Token no está en la base de datos
// @router / [get]
func (c *PROVEEDORESController) GetAll() {
	// Intentar convertir cuerpo de la solicitud
	//var v BodyGetAllProveedores
	//longitudContenidoCuerpo, _ := strconv.Atoi(c.Ctx.Input.Header("Content-Length"))
	//cuerpoSolicitud := c.Ctx.Input.CopyBody(int64(longitudContenidoCuerpo))
	//errorConversionJSON := json.Unmarshal(cuerpoSolicitud, &v)

	token := strings.Split(c.Ctx.Input.Header("Authorization"), " ")[1]
	//token := v.TokenUsuario
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.USUARIOS))

	// Si el token no está en la base de datos
	if !qs.Filter("API_TOKEN", token).Exist() {
		// El solicitante no está autorizado para realizar la operación
		c.Ctx.Output.SetStatus(403)
		c.Data["json"] = "Token no está en la base de datos"
		c.ServeJSON()
		return
	}

	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllPROVEEDORES(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = err.Error()
		c.ServeJSON()
		return
	} else {

		o := orm.NewOrm()
		// Crear slice que contendrá los registros a retornar
		var proveedores []BodyRespuestaGetOneProveedoresParaAll

		for _, proveedor := range l {
			// Leer usuario con la clave primaria del proveedor
			usuario := models.USUARIOS{Id: proveedor.(models.PROVEEDORES).Id}
			o.Read(&usuario)
			// Agregar usuario al slice
			proveedores = append(proveedores,
				BodyRespuestaGetOneProveedoresParaAll{Nombres: usuario.NOMBRES,
					Apellidos: usuario.APELLIDOS, Correo: usuario.CORREO, Id: usuario.Id})
		}

		// Retornar struct correspondiente inicializándola con el slice
		c.Data["json"] = BodyRespuestaGetAllProveedores{Proveedores: proveedores}
	}
	// Solicitud exitosa
	c.Ctx.Output.SetStatus(200)
	c.ServeJSON()
}

// Put ...
// @Title Put
// @Description Modificar el registro de un proveedor identificado por su id
// @Param	id		path 	string	true		"El id del proveedor que se quiere actualizar"
// @Param	body		body 	controllers.BodyPutProveedores	true		"Campos a actualizar (dejar como cadena vacía si el campo no se quiere actualizar) más el token"
// @Success 200 La modificación fue exitosa
// @Failure 400 La solicitud no tiene la estructura correcta o el :id no es un entero.
// @Failure 403 Solicitante no está autorizado para actualizar recurso.
// @Failure 409 Los datos enviados no cumplen con los requisitos de integridad (se retorna una cadena con el error).
// @Failure 500 Solicitud no se pudo llevar a cabo por un error interno en el servidor.
// @router /:id [put]
func (c *PROVEEDORESController) Put() {
	idStr := c.Ctx.Input.Param(":id")
	id, errorIndice := strconv.Atoi(idStr)
	var v BodyPutProveedores
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil && errorIndice == nil {
		var proveedor models.PROVEEDORES
		var usuarioProveedor models.USUARIOS
		tokenProv := v.Token
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))
		qs2 := o.QueryTable(new(models.PROVEEDORES))
		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenProv).One(&usuarioProveedor)
		// Si el token no está en la base de datos o no pertenece a un proveedor
		if errorTokenNoEncontrado != nil || !o.QueryTable(new(models.PROVEEDORES)).Filter("ID_PROVEEDOR", usuarioProveedor.Id).Exist() {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.ServeJSON()
			return
		}
		if id != usuarioProveedor.Id {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.ServeJSON()
			return
		}

		// Inicializar de forma correspondiente (solo asignar los valores que no son cadenas vacías)
		usuarioProveedor.APELLIDOS = (map[bool]string{true: v.Apellidos, false: usuarioProveedor.APELLIDOS})[v.Apellidos != ""]
		// Revisar si se busca un cambio de correo
		cambioDeCorreo := v.Correo != ""
		usuarioProveedor.CORREO = (map[bool]string{true: v.Correo, false: usuarioProveedor.CORREO})[cambioDeCorreo]
		usuarioProveedor.NOMBRES = (map[bool]string{true: v.Nombres, false: usuarioProveedor.NOMBRES})[v.Nombres != ""]
		// Revisar si se busca un cambio de contraseña
		cambioDePass := v.Password != ""
		usuarioProveedor.HASHPASSWORD = (map[bool]string{true: v.Password, false: usuarioProveedor.HASHPASSWORD})[cambioDePass]
		qs2.Filter("ID_PROVEEDOR", usuarioProveedor.Id).One(&proveedor)
		proveedor.ORGANIZACION = (map[bool]string{true: v.Organizacion, false: proveedor.ORGANIZACION})[v.Organizacion != ""]

		// Verificar integridad de la información enviada (solo la parte del usuario)
		if errorIntegridadInfoUsuario := models.RevisarIntegridadUsuario(usuarioProveedor, !cambioDePass, cambioDeCorreo); errorIntegridadInfoUsuario != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(409)
			c.Data["json"] = errorIntegridadInfoUsuario.Error()
			c.ServeJSON()
			return
		} else if errorIntegridadInfoProv := models.RevisarIntegridadProveedor(proveedor); errorIntegridadInfoProv != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(409)
			c.Data["json"] = errorIntegridadInfoProv.Error()
			c.ServeJSON()
			return
		}
		if err := models.UpdateUSUARIOSById(&usuarioProveedor, !cambioDePass); err == nil {
			if err := models.UpdatePROVEEDORESById(&proveedor); err == nil {
				c.Ctx.Output.SetStatus(200)
			} else {
				c.Ctx.Output.SetStatus(500)
			}
		} else {
			c.Ctx.Output.SetStatus(500)
		}
	} else {
		c.Ctx.Output.SetStatus(400)
	}
	c.ServeJSON()
}

// Delete ...
// @Title Delete
// @Description Eliminar proveedor identificado con el id
// @Param	id		path 	string	true		"El id del proveedor que se quiere eliminar"
// @Param	body		body 	controllers.BodyDeleteProveedores	true		"Estructura JSON para enviar en el cuerpo de la solicitud"
// @Success 200 La eliminación fue exitosa
// @Failure 400 La solicitud no tiene la estructura correcta o el :id no es un entero.
// @Failure 403 Solicitante no está autorizado para actualizar recurso.
// @Failure 500 Solicitud no se pudo llevar a cabo por un error interno en el servidor.
// @router /:id [delete]
func (c *PROVEEDORESController) Delete() {
	var v BodyDeleteProveedores
	idStr := c.Ctx.Input.Param(":id")
	id, errorIndice := strconv.Atoi(idStr)
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil && errorIndice == nil {
		var usuarioAdministrador models.USUARIOS
		tokenAdmi := v.Token
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))
		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenAdmi).One(&usuarioAdministrador)
		// Si el token no está en la base de datos o no pertenece a un administrador
		if errorTokenNoEncontrado != nil || !o.QueryTable(new(models.ADMINISTRADORES)).Filter("ID_ADMIN", usuarioAdministrador.Id).Exist() {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.Data["json"] = "Token no válido"
			c.ServeJSON()
			return
		}
		//if id != proveedor.Id {
		// Error en la autenticación de la petición
		//	c.Ctx.Output.SetStatus(403)
		//	c.ServeJSON()
		//	return
		//}
		if err := models.DeletePROVEEDORES(id); err == nil {
			if err := models.DeleteUSUARIOS(id); err == nil {
				c.Ctx.Output.SetStatus(200)
			} else {
				c.Ctx.Output.SetStatus(500)
			}
		} else {
			c.Ctx.Output.SetStatus(500)
		}
	} else {
		c.Ctx.Output.SetStatus(400)
	}
	c.ServeJSON()
}
