package controllers

import (
	"backendmarketplace/models"
	"encoding/json"
	"errors"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

// CLIENTESController operations for CLIENTES
type CLIENTESController struct {
	beego.Controller
}

// Estructura para parsear el cuerpo de una solicitud POST para la creación de un CLIENTE
type BODY_POST_CLIENTES struct {
	Apellidos string
	Correo    string
	Password  string
	Nombres   string
}

// Estructura que contiene la respuesta a una solicitud POST para la creación de un CLIENTE
type BODY_RESPUESTA_POST_CLIENTES struct {
	Id int
}

// BodyGetAllClientes Estructura para parsear el cuerpo de una solicitud GET para la lectura de los clientes
//type BodyGetAllClientes struct {
//	TokenUsuario string
//}

// BodyGetOneClientes Estructura para parsear el cuerpo de una solicitud GET para la lectura de un registro
// particular de un cliente con el :id enviado
//type BodyGetOneClientes = BodyGetAllClientes

// BodyRespuestaGetOneClientes Estructura que contiene la respuesta a una
// solicitud GET para la obtención de la información de un CLIENTE en particular
type BodyRespuestaGetOneClientes struct {
	Nombres   string
	Apellidos string
	Correo    string
}

// BodyRespuestaGetOneClientesParaAll Estructura que contiene la respuesta de un
// solo registro que compone la respuesta para BodyRespuestaGetAllClientes
type BodyRespuestaGetOneClientesParaAll struct {
	Nombres   string
	Apellidos string
	Correo    string
	Id        int
}

// BodyRespuestaGetAllClientes Estructura que contiene la respuesta a una
// solicitud GET para la obtención de la información de todos los CLIENTES
type BodyRespuestaGetAllClientes struct {
	Clientes []BodyRespuestaGetOneClientesParaAll
}

// BodyPutClientes Estructura para parsear el cuerpo de una solicitud PUT para la modificación de un CLIENTE
type BodyPutClientes struct {
	Apellidos string
	Correo    string
	Password  string
	Nombres   string
	Token  string
}

// BodyDeleteClientes Estructura que contiene el cuerpo de una solicitud DELETE para la eliminación de un CLIENTE
type BodyDeleteClientes struct {
	Token string
}

// URLMapping ...
func (c *CLIENTESController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Post
// @Description Crear un nuevo registro de un cliente
// @Param	body		body 	controllers.BODY_POST_CLIENTES	true		"Estructura del JSON del cuerpo de la solicitud"
// @Success 201 {object} controllers.BODY_RESPUESTA_POST_CLIENTES
// @Failure 400 Error en los datos (se retorna cadena explicativa) o estructura de la solicitud incorrecta
// @Failure 500 Error en el servidor
// @router / [post]
func (c *CLIENTESController) Post() {
	var v BODY_POST_CLIENTES
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {

		// Crear objetos cliente y usuario de acuerdo a la solicitud realizada
		var usuario models.USUARIOS
		var cliente models.CLIENTES

		// Inicializar de forma correspondiente
		usuario.APELLIDOS = v.Apellidos
		usuario.CORREO = v.Correo
		usuario.NOMBRES = v.Nombres
		usuario.HASHPASSWORD = v.Password

		// Verificar integridad de la información enviada
		if errorIntegridad := models.RevisarIntegridadUsuario(usuario, false, true); errorIntegridad != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(400)
			c.Data["json"] = errorIntegridad.Error()
			c.ServeJSON()
			return
		}

		// Insertar registro del usuario
		if id_usuario, error_insercion_usuario := models.AddUSUARIOS(&usuario); error_insercion_usuario == nil {
			// Asignar mismo id al cliente
			cliente.Id = int(id_usuario)
			// Insertar registro del cliente (será exitoso porque el de usuarios lo fue)
			models.AddCLIENTES(&cliente)
			// Creación exitosa
			c.Ctx.Output.SetStatus(201)
			c.Data["json"] = BODY_RESPUESTA_POST_CLIENTES{Id: int(id_usuario)}
		} else {
			// Error en la inserción del usuario que no está relacionada con la integridad de los datos
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = "Error en el servidor"
		}
	} else {
		// Error en la estructura del cuerpo de la solicitud
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Estructura de la solicitud incorrecta"
	}
	c.ServeJSON()
}

// GetOne ...
// @Title Get One
// @Description Obtener el registro de un cliente usando su :id (usar el header "Authorization" así "Bearer Token")
// @Param	id		path 	string	true		"El :id del registro del cliente cuya información se quiere obtener"
// @Success 200 {object} controllers.BodyRespuestaGetOneClientes
// @Failure 400 El parámetro :id no es un entero o la estructura del cuerpo de la solicitud no es correcta
// @Failure 403 Token no está en la base de datos
// @Failure 404 No existe registro con ese :id
// @router /:id [get]
func (c *CLIENTESController) GetOne() {

	// Obtener :id
	idStr := c.Ctx.Input.Param(":id")
	id, errID := strconv.Atoi(idStr)

	// Revisar si hubo un error al convertir el :id
	if errID != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "El parámetro :id no es un entero"
		c.ServeJSON()
		return
	}

	// Intentar convertir cuerpo de la solicitud
	token := strings.Split(c.Ctx.Input.Header("Authorization"), " ")[1]
	//var v BodyGetOneClientes
	//longitudContenidoCuerpo, _ := strconv.Atoi(c.Ctx.Input.Header("Content-Length"))
	//cuerpoSolicitud := c.Ctx.Input.CopyBody(int64(longitudContenidoCuerpo))
	//errorConversionJSON := json.Unmarshal(cuerpoSolicitud, &v)

	//if errorConversionJSON == nil {

	//	token := v.TokenUsuario
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.USUARIOS))

	// Si el token no está en la base de datos
	if !qs.Filter("API_TOKEN", token).Exist() {
		// El solicitante no está autorizado para realizar la operación
		c.Ctx.Output.SetStatus(403)
		c.Data["json"] = "Token no está en la base de datos"
	} else {
		// Si el token es correcto
		v, errorIDNoExiste := models.GetCLIENTESById(id)
		if errorIDNoExiste != nil {
			// El id enviado no existe
			c.Ctx.Output.SetStatus(404)
			c.Data["json"] = "Recurso no existe"
		} else {
			// Lectura exitosa
			registroUsuario, _ := models.GetUSUARIOSById(v.Id)
			c.Ctx.Output.SetStatus(200)
			c.Data["json"] = BodyRespuestaGetOneClientes{Apellidos: registroUsuario.APELLIDOS,
				Nombres: registroUsuario.NOMBRES,
				Correo:  registroUsuario.CORREO}
		}
	}
	//} else {
	// Error en la estructura del body de la solicitud enviada
	//	c.Ctx.Output.SetStatus(400)
	//	c.Data["json"] = "Error al procesar la estructura del cuerpo de la solicitud"
	//}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description Obtener todos los registros de clientes en la base de datos (usar el header "Authorization" así "Bearer Token")
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} controllers.BodyRespuestaGetAllClientes
// @Failure 400 Error en la estructura del body de la solicitud enviada o en la query string
// @Failure 403 Token no está en la base de datos
// @router / [get]
func (c *CLIENTESController) GetAll() {

	// Intentar convertir cuerpo de la solicitud
	// var v BodyGetAllClientes
	// longitudContenidoCuerpo, _ := strconv.Atoi(c.Ctx.Input.Header("Content-Length"))
	// cuerpoSolicitud := c.Ctx.Input.CopyBody(int64(longitudContenidoCuerpo))
	// errorConversionJSON := json.Unmarshal(cuerpoSolicitud, &v)

	token := strings.Split(c.Ctx.Input.Header("Authorization"), " ")[1]
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.USUARIOS))

	// Si el token no está en la base de datos
	if !qs.Filter("API_TOKEN", token).Exist() {
		// El solicitante no está autorizado para realizar la operación
		c.Ctx.Output.SetStatus(403)
		c.Data["json"] = "Token no está en la base de datos"
		c.ServeJSON()
		return
	}

	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				// Este es un error en la query string de la solicitud
				c.Ctx.Output.SetStatus(400)
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllCLIENTES(query, fields, sortby, order, offset, limit)
	if err != nil {
		// La mayoría de los errores tienen que ver con los valores de la query string
		// Hay uno que no se ha podido identificar qué significa (cuándo ocurre). Ocurre en la línea:
		// _, err = qs.Limit(limit, offset).All(&l, fields...)
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = err.Error()
		c.ServeJSON()
		return
	} else {

		o := orm.NewOrm()
		// Crear slice que contendrá los registros a retornar
		var clientes []BodyRespuestaGetOneClientesParaAll

		for _, cliente := range l {
			// Leer usuario con la clave primaria del cliente
			usuario := models.USUARIOS{Id: cliente.(models.CLIENTES).Id}
			o.Read(&usuario)
			// Agregar usuario al slice
			clientes = append(clientes,
				BodyRespuestaGetOneClientesParaAll{Nombres: usuario.NOMBRES,
					Apellidos: usuario.APELLIDOS, Correo: usuario.CORREO, Id: usuario.Id})
		}

		// Retornar struct correspondiente inicializándola con el slice
		c.Data["json"] = BodyRespuestaGetAllClientes{Clientes: clientes}
	}

	// Solicitud exitosa
	c.Ctx.Output.SetStatus(200)
	c.ServeJSON()
}

// Put ...
// @Title Put
// @Description Modificar el registro de un cliente identificado por su id
// @Param	id		path 	string	true		"El id del cliente que se quiere actualizar"
// @Param	body		body 	controllers.BodyPutClientes	true		"Campos a actualizar (dejar como cadena vacía si el campo no se quiere actualizar) más el token del cliente dueño de la cuenta"
// @Success 200 La modificación fue exitosa.
// @Failure 400 La solicitud no tiene la estructura correcta o el :id no es un entero.
// @Failure 403 Solicitante no está autorizado para actualizar recurso.
// @Failure 409 Los datos enviados no cumplen con los requisitos de integridad (se retorna una cadena que indica el error).
// @Failure 500 Solicitud no se pudo llevar a cabo por un error interno en el servidor.
// @router /:id [put]
func (c *CLIENTESController) Put() {

	// Obtener id del cliente a eliminar
	idStr := c.Ctx.Input.Param(":id")
	// Convertir id a entero
	id, errorIndice := strconv.Atoi(idStr)

	// Revisar si hubo un error en la conversión del id
	if errorIndice != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Error en la estructura del id en la URL"
		c.ServeJSON()
		return
	}

	// Intentar convertir cuerpo de la solicitud
	var v BodyPutClientes
	if errorConversionJSON := json.Unmarshal(c.Ctx.Input.RequestBody, &v); errorConversionJSON == nil {

		var usuarioCliente models.USUARIOS
		tokenCli := v.Token
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))

		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenCli).One(&usuarioCliente)
		// Si el token no está en la base de datos, no pertenece a un cliente
		// o pertenece a otro cliente
		if errorTokenNoEncontrado != nil ||
			!o.QueryTable(new(models.CLIENTES)).Filter("ID_USUARIO", usuarioCliente.Id).Exist() ||
			id != usuarioCliente.Id {
			// El solicitante no está autorizado para realizar la operación
			c.Ctx.Output.SetStatus(403)
			c.Data["json"] = "Solicitante no está autorizado para actualizar recurso"
			c.ServeJSON()
			return
		}

		// Inicializar de forma correspondiente (solo asignar los valores que no son cadenas vacías)
		usuarioCliente.APELLIDOS = (map[bool]string{true: v.Apellidos, false: usuarioCliente.APELLIDOS})[v.Apellidos != ""]
		// Revisar si se busca cambiar el correo
		cambioCorreo := v.Correo != ""
		usuarioCliente.CORREO = (map[bool]string{true: v.Correo, false: usuarioCliente.CORREO})[cambioCorreo]
		usuarioCliente.NOMBRES = (map[bool]string{true: v.Nombres, false: usuarioCliente.NOMBRES})[v.Nombres != ""]
		// Revisar si se busca un cambio de contraseña
		cambioDePass := v.Password != ""
		usuarioCliente.HASHPASSWORD = (map[bool]string{true: v.Password, false: usuarioCliente.HASHPASSWORD})[cambioDePass]

		// Verificar integridad de la información enviada
		if errorIntegridadInfoUsuario := models.RevisarIntegridadUsuario(usuarioCliente, !cambioDePass, cambioCorreo); errorIntegridadInfoUsuario != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(409)
			c.Data["json"] = errorIntegridadInfoUsuario.Error()
			c.ServeJSON()
			return
		}

		// Actualizar información
		if errorActualizacionUsuario := models.UpdateUSUARIOSById(&usuarioCliente, !cambioDePass); errorActualizacionUsuario == nil {
			c.Ctx.Output.SetStatus(200)
		} else {
			// Error al actualizar el registro que no está relacionado con la integridad de los datos
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = "Error interno del servidor"
		}
	} else {
		// Error en la estructura del body de la solicitud enviada
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Estructura de la solicitud incorrecta"
	}
	c.ServeJSON()
}

// Delete ...
// @Title Delete
// @Description Eliminar el registro de un cliente identificado por su id
// @Param	id		path 	string	true		"El id del cliente que se quiere eliminar"
// @Param	body		body 	controllers.BodyDeleteClientes	true		"Estructura JSON para enviar en el cuerpo de la solicitud (el token debe ser de un administrador)"
// @Success 200 La eliminación fue exitosa
// @Failure 400 La solicitud no tiene la estructura correcta o el :id no es un entero.
// @Failure 403 Solicitante no está autorizado para eliminar el recurso.
// @Failure 404 No existe el registro que se quiere eliminar
// @Failure 500 Solicitud no se pudo llevar a cabo por un error interno en el servidor.
// @router /:id [delete]
func (c *CLIENTESController) Delete() {

	// Obtener id del cliente a eliminar
	idStr := c.Ctx.Input.Param(":id")
	// Convertir id a entero
	id, errorIndice := strconv.Atoi(idStr)

	// Revisar si hubo un error en la conversión del id
	if errorIndice != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Error en la estructura del id en la URL"
		c.ServeJSON()
		return
	}

	// Intentar convertir cuerpo de la solicitud
	var v BodyDeleteClientes
	if errorConversionJSON := json.Unmarshal(c.Ctx.Input.RequestBody, &v); errorConversionJSON == nil {
		var adminSolicitante models.USUARIOS
		tokenAdmi := v.Token
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))
		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenAdmi).One(&adminSolicitante)
		// Si el token no está en la base de datos o no pertenece a un administrador
		if errorTokenNoEncontrado != nil || !o.QueryTable(new(models.ADMINISTRADORES)).Filter("ID_ADMIN", adminSolicitante.Id).Exist() {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.Data["json"] = "Token no válido"
			c.ServeJSON()
			return
		}

		// Revisar si existe el recurso a eliminar
		if _, errorNoExisteClienteAEliminar := models.GetCLIENTESById(id); errorNoExisteClienteAEliminar != nil {
			c.Ctx.Output.SetStatus(404)
			c.Data["json"] = "Recurso a eliminar no existe"
			c.ServeJSON()
			return
		}

		// Realizar la eliminación
		if errorEliminacionCliente := models.DeleteCLIENTES(id); errorEliminacionCliente == nil {
			if errorEliminacionUsuario := models.DeleteUSUARIOS(id); errorEliminacionUsuario == nil {
				c.Ctx.Output.SetStatus(200)
				c.ServeJSON()
				return
			}
		}
		// Hubo un error con la base de datos al momento de eliminar el registro
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = "Error interno del servidor"
	} else {
		// No se pudo convertir el cuerpo de la solicitud a la struct esperada
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Estructura de la solicitud incorrecta"
	}
	c.ServeJSON()
}
