package controllers

import (
	"backendmarketplace/models"
	"encoding/json"
	"errors"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

// CATEGORIASController operations for CATEGORIAS
type CATEGORIASController struct {
	beego.Controller
}

// BodyPostCategorias representa el cuerpo de la solicitud POST para crear una categoría.
type BodyPostCategorias struct {
	TituloCategoria string
	Token           string
}

// BodyRespuestaPostCategorias representa el cuerpo de respuesta a una solicitud POST para crear una categoría
type BodyRespuestaPostCategorias struct {
	Id int
}

// BodyRespuestaGetOneCategorias representa el cuerpo de respuesta a solicitud GET para leer una categoría específica
type BodyRespuestaGetOneCategorias struct {
	TituloCategoria string
	Id              int
}

// BodyRespuestaGetAllCategorias Estructura que contiene la respuesta a una
// solicitud GET para la obtención de la información de todas las CATEGORÍAS
type BodyRespuestaGetAllCategorias struct {
	Categorias []BodyRespuestaGetOneCategorias
}

// URLMapping ...
func (c *CATEGORIASController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Post
// @Description Crear un nuevo registro de una categoria (solo un proveedor puede crear una categoría)
// @Param	body		body 	controllers.BodyPostCategorias	true		"Estructura del JSON para enviar en el cuerpo de la solicitud"
// @Success 201 {object} controllers.BodyRespuestaPostCategorias
// @Failure 400 Error en los datos (el título de la categoría es igual a uno que ya existe al no distinguir entre mayúsculas y minúsculas)
// @Failure 403 Token no válido
// @Failure 500 Error en el servidor
// @router / [post]
func (c *CATEGORIASController) Post() {

	var v BodyPostCategorias
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {
		var usuarioProveedor models.USUARIOS
		tokenProv := v.Token
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))
		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenProv).One(&usuarioProveedor)

		// Si el token no está en la base de datos o no pertenece a un proveedor
		if errorTokenNoEncontrado != nil || !o.QueryTable(new(models.PROVEEDORES)).Filter("ID_PROVEEDOR", usuarioProveedor.Id).Exist() {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.Data["json"] = "Token no válido"
			c.ServeJSON()
			return
		}
		// Crear objeto categoría
		var categoria models.CATEGORIAS

		// Inicializar campos en el cuerpo de la solicitud
		categoria.TITULOCATEGORIA = v.TituloCategoria

		// Revisar integridad de la categoría
		if errorIntegridadCategoria := models.RevisarIntegridadCategorias(categoria); errorIntegridadCategoria != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(400)
			c.Data["json"] = errorIntegridadCategoria.Error()
			c.ServeJSON()
			return
		}

		// Insertar registro de la categoría
		if id_categoria, error_insercion_categoria := models.AddCATEGORIAS(&categoria); error_insercion_categoria == nil {
			// Se insertó la categoría
			c.Ctx.Output.SetStatus(201)
			c.Data["json"] = BodyRespuestaPostCategorias{Id: int(id_categoria)}
		} else {
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = err.Error()
		}
		c.ServeJSON()
	}
}

// GetOne ...
// @Title Get One
// @Description Obtener el registro de un paquete usando su :id (usar el header "Authorization" así "Bearer Token"). El token debe ser de un usuario registrado.
// @Param	id		path 	string	true		"El :id del registro de la categoría cuya información se quiere obtener"
// @Success 200 {object} controllers.BodyRespuestaGetOneCategorias
// @Failure 400 El parámetro :id no es un entero o la estructura del cuerpo de la solicitud no es correcta
// @Failure 403 Token no está en la base de datos
// @Failure 404 No existe registro con ese :id
// @router /:id [get]
func (c *CATEGORIASController) GetOne() {
	// Obtener :id
	idStr := c.Ctx.Input.Param(":id")
	id, errID := strconv.Atoi(idStr)
	// Revisar si hubo un error al convertir el :id
	if errID != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "El parámetro :id no es un entero"
		c.ServeJSON()
		return
	}
	//Token
	token := strings.Split(c.Ctx.Input.Header("Authorization"), " ")[1]
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.USUARIOS))
	// Si el token no está en la base de datos
	if !qs.Filter("API_TOKEN", token).Exist() {
		// El solicitante no está autorizado para realizar la operación
		c.Ctx.Output.SetStatus(403)
		c.Data["json"] = "Token no está en la base de datos"
		c.ServeJSON()
		return
	}
	datosCategoria, errorIDNoExiste := models.GetCATEGORIASById(id)
	if errorIDNoExiste != nil {
		// El id enviado no existe
		c.Ctx.Output.SetStatus(404)
		c.Data["json"] = "Recurso no existe"
	} else {
		// Lectura exitosa
		// Obtener los datos de la categoría
		c.Ctx.Output.SetStatus(200)
		c.Data["json"] = BodyRespuestaGetOneCategorias{Id: datosCategoria.Id, TituloCategoria: datosCategoria.TITULOCATEGORIA}
	}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description Obtener todos los registros de categorías en la base de datos (usar el header "Authorization" así "Bearer Token"). El token debe ser de un usuario registrado.
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} controllers.BodyRespuestaGetAllCategorias
// @Failure 400 Error en la estructura del body de la solicitud enviada o en la query string
// @Failure 403 Token no está en la base de datos
// @router / [get]
func (c *CATEGORIASController) GetAll() {
	//Token
	token := strings.Split(c.Ctx.Input.Header("Authorization"), " ")[1]
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.USUARIOS))

	// Si el token no está en la base de datos
	if !qs.Filter("API_TOKEN", token).Exist() {
		// El solicitante no está autorizado para realizar la operación
		c.Ctx.Output.SetStatus(403)
		c.Data["json"] = "Token no está en la base de datos"
		c.ServeJSON()
		return
	}

	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllCATEGORIAS(query, fields, sortby, order, offset, limit)
	if err != nil {
		// La mayoría de los errores tienen que ver con los valores de la query string
		// Hay uno que no se ha podido identificar qué significa (cuándo ocurre). Ocurre en la línea:
		// _, err = qs.Limit(limit, offset).All(&l, fields...)
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = err.Error()
		c.ServeJSON()
		return
	} else {
		// Crear slice que contendrá los registros a retornar
		var categorias []BodyRespuestaGetOneCategorias

		for _, categoria := range l {
			datosCategoria := categoria.(models.CATEGORIAS)
			// Guardar los datos del paquete
			categorias = append(categorias,
				BodyRespuestaGetOneCategorias{Id: datosCategoria.Id, TituloCategoria: datosCategoria.TITULOCATEGORIA})
		}

		// Retornar struct correspondiente inicializándola con el slice
		c.Data["json"] = BodyRespuestaGetAllCategorias{Categorias: categorias}
	}
	c.ServeJSON()
}

func (c *CATEGORIASController) Put() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	v := models.CATEGORIAS{Id: id}
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {
		if err := models.UpdateCATEGORIASById(&v); err == nil {
			c.Data["json"] = "OK"
		} else {
			c.Data["json"] = err.Error()
		}
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}

func (c *CATEGORIASController) Delete() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)
	if err := models.DeleteCATEGORIAS(id); err == nil {
		c.Data["json"] = "OK"
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}
