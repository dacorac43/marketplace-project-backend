package controllers

import (
	"backendmarketplace/models"
	"encoding/json"
	"errors"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

// ADMINISTRADORESController operations for ADMINISTRADORES
type ADMINISTRADORESController struct {
	beego.Controller
}

// BODY_POST_ADMINISTRADORES Estructura para parsear el cuerpo de una solicitud POST para la creación de un ADMINISTRADOR
type BODY_POST_ADMINISTRADORES struct {
	Apellidos string
	Correo    string
	Password  string
	Nombres   string
	Token     string
}

// BodyPutAdministradores Estructura para parsear el cuerpo de una solicitud PUT para la modificación de un ADMINISTRADOR
type BodyPutAdministradores = BODY_POST_ADMINISTRADORES

// BODY_RESPUESTA_POST_ADMINISTRADORES Estructura que contiene la respuesta a una
// solicitud POST para la creación de un ADMINISTRADOR
type BODY_RESPUESTA_POST_ADMINISTRADORES struct {
	Id int
}

// BodyRespuestaGetOneAdministradores Estructura que contiene la respuesta a una
// solicitud GET para la obtención de la información de un ADMINISTRADOR en particular
type BodyRespuestaGetOneAdministradores struct {
	Nombres   string
	Apellidos string
	Correo    string
}

// BodyRespuestaGetOneAdministradoresParaAll Estructura que contiene la respuesta de un
// solo registro que compone la respuesta para BodyRespuestaGetAllAdministradores
type BodyRespuestaGetOneAdministradoresParaAll struct {
	Nombres   string
	Apellidos string
	Correo    string
	Id        int
}

// BodyRespuestaGetAllAdministradores Estructura que contiene la respuesta a una
// solicitud GET para la obtención de la información de todos los ADMINISTRADORES
type BodyRespuestaGetAllAdministradores struct {
	Administradores []BodyRespuestaGetOneAdministradoresParaAll
}

// BodyDeleteAdministradores Estructura que contiene el cuerpo de una solicitud DELETE para la eliminación de un ADMINISTRADOR
type BodyDeleteAdministradores struct {
	Token string
}

// URLMapping ...
func (c *ADMINISTRADORESController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Accept json
// @Title Post
// @Description Crear un nuevo registro de administrador
// @Param	datos		body 	controllers.BODY_POST_ADMINISTRADORES	true		"Estructura del JSON para enviar en el cuerpo de la solicitud (el token enviado debe ser de un administrador)"
// @Success 201 {object} controllers.BODY_RESPUESTA_POST_ADMINISTRADORES
// @Failure 400 Error en los datos o estructura de la solicitud incorrecta
// @Failure 403 Token no válido (token no está en la base de datos o no pertenece a un administrador)
// @Failure 500 Error en el servidor
// @router / [post]
func (c *ADMINISTRADORESController) Post() {
	var v BODY_POST_ADMINISTRADORES
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {

		// Revisar token
		var usuarioAdministrador models.USUARIOS
		tokenAdmin := v.Token
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))
		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenAdmin).One(&usuarioAdministrador)

		// Si el token no está en la base de datos o no pertenece a un administrador
		if errorTokenNoEncontrado != nil || !o.QueryTable(new(models.ADMINISTRADORES)).Filter("ID_ADMIN", usuarioAdministrador.Id).Exist() {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.Data["json"] = "Token no válido"
			c.ServeJSON()
			return
		}

		// Crear objetos administrador y usuario de acuerdo a la solicitud realizada
		var usuario models.USUARIOS
		var administrador models.ADMINISTRADORES

		// Inicializar de forma correspondiente
		usuario.APELLIDOS = v.Apellidos
		usuario.CORREO = v.Correo
		usuario.NOMBRES = v.Nombres
		usuario.HASHPASSWORD = v.Password

		// Verificar integridad de la información enviada
		if errorIntegridad := models.RevisarIntegridadUsuario(usuario, false, true); errorIntegridad != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(400)
			c.Data["json"] = errorIntegridad.Error()
			c.ServeJSON()
			return
		}

		// Insertar registro del usuario
		if idUsuario, errorInsercionUsuario := models.AddUSUARIOS(&usuario); errorInsercionUsuario == nil {
			// Asignar mismo id al administrador
			administrador.Id = int(idUsuario)
			// Isertar registro del administrador (será exitoso porque el de usuarios lo fue)
			models.AddADMINISTRADORES(&administrador)
			// Creación exitosa
			c.Ctx.Output.SetStatus(201)
			c.Data["json"] = BODY_RESPUESTA_POST_ADMINISTRADORES{Id: int(idUsuario)}
		} else {
			// Error al ingresar el registro que no está relacionado con la integridad de los datos
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = "Error en el servidor"
		}
	} else {
		// Error en la estructura del cuerpo de la solicitud
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Estructura de la solicitud incorrecta"
	}
	c.ServeJSON()
}

// GetOne ...
// @Title Get One
// @Description @Description Obtener el registro de un administrador usando su :id
// @Param	id		path 	string	true		"El :id del registro del administrador cuya información se quiere obtener"
// @Success 200 {object} controllers.BodyRespuestaGetOneAdministradores
// @Failure 400 El parámetro :id no es un entero
// @Failure 404 No existe registro con ese :id
// @router /:id [get]
func (c *ADMINISTRADORESController) GetOne() {

	// Obtener :id
	idStr := c.Ctx.Input.Param(":id")
	id, errID := strconv.Atoi(idStr)

	// Revisar si hubo un error al convertir el :id
	if errID != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "El parámetro :id no es un entero"
		c.ServeJSON()
		return
	}

	v, errorIDNoExiste := models.GetADMINISTRADORESById(id)
	if errorIDNoExiste != nil {
		// El id enviado no existe
		c.Ctx.Output.SetStatus(404)
		c.Data["json"] = "Recurso no existe"
	} else {
		// Lectura exitosa
		registroUsuario, _ := models.GetUSUARIOSById(v.Id)
		c.Ctx.Output.SetStatus(200)
		c.Data["json"] = BodyRespuestaGetOneAdministradores{Apellidos: registroUsuario.APELLIDOS,
			Nombres: registroUsuario.NOMBRES,
			Correo:  registroUsuario.CORREO}
	}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description Leer la información de todos los administradores
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} controllers.BodyRespuestaGetAllAdministradores
// @Failure 400 Error en la solicitud (retorna una cadena descriptiva del error)
// @router / [get]
func (c *ADMINISTRADORESController) GetAll() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				// Retornar mensaje de error apropiado
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				// Retornar código que indica el fallo en la solicitud
				c.Ctx.Output.SetStatus(400)
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllADMINISTRADORES(query, fields, sortby, order, offset, limit)
	if err != nil {
		// Retornar mensaje de error apropiado
		c.Data["json"] = err.Error()
		// Retornar código que indica el fallo en la solicitud
		c.Ctx.Output.SetStatus(400)
	} else {

		o := orm.NewOrm()
		// Crear slice que contendrá los registros a retornar
		var administradores []BodyRespuestaGetOneAdministradoresParaAll

		for _, administrador := range l {
			// Leer usuario con la clave primaria del administrador
			usuario := models.USUARIOS{Id: administrador.(models.ADMINISTRADORES).Id}
			o.Read(&usuario)
			// Agregar usuario al slice
			administradores = append(administradores,
				BodyRespuestaGetOneAdministradoresParaAll{Nombres: usuario.NOMBRES,
					Apellidos: usuario.APELLIDOS, Correo: usuario.CORREO, Id: usuario.Id})
		}

		// Retornar struct correspondiente inicializándola con el slice
		c.Data["json"] = BodyRespuestaGetAllAdministradores{Administradores: administradores}
		// Retornar código que indica el éxito
		c.Ctx.Output.SetStatus(200)
	}
	c.ServeJSON()
}

// Put ...
// @Title Put
// @Description Modificar el registro de un administrador identificado por su id
// @Param	id		path 	string	true		"El id del administrador que se quiere actualizar"
// @Param	body		body 	controllers.BODY_POST_ADMINISTRADORES	true		"Campos a actualizar (dejar como cadena vacía si el campo no se quiere actualizar) más el token del administrador dueño de la cuenta"
// @Success 200 La modificación fue exitosa.
// @Failure 400 La solicitud no tiene la estructura correcta o el :id no es un entero.
// @Failure 403 Solicitante no está autorizado para actualizar recurso.
// @Failure 409 Los datos enviados no cumplen con los requisitos de integridad (se retorna una cadena que indica el error).
// @Failure 500 Solicitud no se pudo llevar a cabo por un error interno en el servidor.
// @router /:id [put]
func (c *ADMINISTRADORESController) Put() {

	// Obtener id del administrador a modificar
	idStr := c.Ctx.Input.Param(":id")
	// Convertir id a entero
	id, errorIndice := strconv.Atoi(idStr)

	// Revisar si hubo un error en la conversión del id
	if errorIndice != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Error en la estructura del id en la URL"
		c.ServeJSON()
		return
	}

	// Intentar convertir cuerpo de la solicitud
	var v BodyPutAdministradores
	if errorConversionJSON := json.Unmarshal(c.Ctx.Input.RequestBody, &v); errorConversionJSON == nil {

		var usuarioAdministrador models.USUARIOS
		tokenAdmin := v.Token
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))

		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenAdmin).One(&usuarioAdministrador)
		// Si el token no está en la base de datos, no pertenece a un administrador
		// o pertenece a otro administrador
		if errorTokenNoEncontrado != nil ||
			!o.QueryTable(new(models.ADMINISTRADORES)).Filter("ID_ADMIN", usuarioAdministrador.Id).Exist() ||
			id != usuarioAdministrador.Id {
			// El solicitante no está autorizado para realizar la operación
			c.Ctx.Output.SetStatus(403)
			c.Data["json"] = "Solicitante no está autorizado para actualizar recurso"
			c.ServeJSON()
			return
		}

		// Inicializar de forma correspondiente (solo asignar los valores que no son cadenas vacías)
		usuarioAdministrador.APELLIDOS = (map[bool]string{true: v.Apellidos, false: usuarioAdministrador.APELLIDOS})[v.Apellidos != ""]
		// Revisar si se busca cambiar el correo
		cambioCorreo := v.Correo != ""
		usuarioAdministrador.CORREO = (map[bool]string{true: v.Correo, false: usuarioAdministrador.CORREO})[cambioCorreo]
		usuarioAdministrador.NOMBRES = (map[bool]string{true: v.Nombres, false: usuarioAdministrador.NOMBRES})[v.Nombres != ""]
		// Revisar si se busca un cambio de contraseña
		cambioDePass := v.Password != ""
		usuarioAdministrador.HASHPASSWORD = (map[bool]string{true: v.Password, false: usuarioAdministrador.HASHPASSWORD})[cambioDePass]

		// Verificar integridad de la información enviada
		if errorIntegridadInfoUsuario := models.RevisarIntegridadUsuario(usuarioAdministrador, !cambioDePass, cambioCorreo); errorIntegridadInfoUsuario != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(409)
			c.Data["json"] = errorIntegridadInfoUsuario.Error()
			c.ServeJSON()
			return
		}

		// Actualizar información
		if errorActualizacionUsuario := models.UpdateUSUARIOSById(&usuarioAdministrador, !cambioDePass); errorActualizacionUsuario == nil {
			c.Ctx.Output.SetStatus(200)
		} else {
			// Error al actualizar el registro que no está relacionado con la integridad de los datos
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = "Error interno del servidor"
		}
	} else {
		// Error en la estructura del body de la solicitud enviada
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Estructura de la solicitud incorrecta"
	}
	c.ServeJSON()
}

// Delete ...
// @Title Delete
// @Description Eliminar el registro de un administrador identificado por su id
// @Param	id		path 	string	true		"El id del administrador que se quiere eliminar"
// @Param	body		body 	controllers.BodyDeleteAdministradores	true		"Estructura JSON para enviar en el cuerpo de la solicitud (el token debe ser del administrador dueño de la cuenta)"
// @Success 200 La eliminación fue exitosa
// @Failure 400 La solicitud no tiene la estructura correcta o el :id no es un entero.
// @Failure 403 Solicitante no está autorizado para eliminar recurso.
// @Failure 500 Solicitud no se pudo llevar a cabo por un error interno en el servidor.
// @router /:id [delete]
func (c *ADMINISTRADORESController) Delete() {

	// Obtener id del administrador a eliminar
	idStr := c.Ctx.Input.Param(":id")
	// Convertir id a entero
	id, errorIndice := strconv.Atoi(idStr)

	// Revisar si hubo un error en la conversión del id
	if errorIndice != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Error en la estructura del id en la URL"
		c.ServeJSON()
		return
	}

	// Intentar convertir cuerpo de la solicitud
	var v BodyDeleteAdministradores
	if errorConversionJSON := json.Unmarshal(c.Ctx.Input.RequestBody, &v); errorConversionJSON == nil {
		var usuarioAdministrador models.USUARIOS
		tokenAdmi := v.Token
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))
		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenAdmi).One(&usuarioAdministrador)
		// Si el token no está en la base de datos o no pertenece a un administrador
		if errorTokenNoEncontrado != nil || !o.QueryTable(new(models.ADMINISTRADORES)).Filter("ID_ADMIN", usuarioAdministrador.Id).Exist() {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.Data["json"] = "Token no válido"
			c.ServeJSON()
			return
		}

		if id != usuarioAdministrador.Id {
			// El administrador logueado que hace la solicitud quiere eliminar otro administrador
			c.Ctx.Output.SetStatus(403)
			c.Data["json"] = "No tiene permisos para hacer la operación"
			c.ServeJSON()
			return
		}

		// Realizar la eliminación
		if errorEliminacionAdministrador := models.DeleteADMINISTRADORES(id); errorEliminacionAdministrador == nil {
			if errorEliminacionUsuario := models.DeleteUSUARIOS(id); errorEliminacionUsuario == nil {
				c.Ctx.Output.SetStatus(200)
				c.ServeJSON()
				return
			}
		}
		// Hubo un error con la base de datos al momento de eliminar el registro
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = "Error interno del servidor"
	} else {
		// No se pudo convertir el cuerpo de la solicitud a la struct esperada
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Estructura de la solicitud incorrecta"
	}
	c.ServeJSON()
}
