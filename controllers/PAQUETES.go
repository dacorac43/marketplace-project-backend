package controllers

import (
	"backendmarketplace/models"
	"encoding/json"
	"errors"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

// PAQUETESController operations for PAQUETES
type PAQUETESController struct {
	beego.Controller
}

type BodyPostPaquetes struct {
	CiudadDestino   string
	Precio          float32
	CuposPublicados int
	Servicios       []BodyPostServicios
	TokenProv       string
}
type BodyPutPaquetes struct {
	CiudadDestino    string
	Precio           float32
	CuposDisponibles int
	Servicios        []BodyPutServicios
	TokenProv        string
}
type BodyRespuestaGetOnePaquetes struct {
	CiudadDestino    string
	Precio           float32
	Proveedor        BodyRespuestaGetOneProveedoresParaAll
	CuposDisponibles int
	CuposPublicados  int
	Servicios        BodyRespuestaGetAllServicios
	Id               int
}

type BodyRespuestaPostPaquetes struct {
	Id int
}

// BodyRespuestaGetAllPaquetes Estructura que contiene la respuesta a una
// solicitud GET para la obtención de la información de todos los PAQUETES
type BodyRespuestaGetAllPaquetes struct {
	Paquetes []BodyRespuestaGetOnePaquetes
}

type BodyDeletePaquetes struct {
	TokenProv string
}

// URLMapping ...
func (c *PAQUETESController) URLMapping() {
	c.Mapping("Post", c.Post)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Post
// @Description Crear un nuevo registro de paquetes
// @Param	body		body 	controllers.BodyPostPaquetes	true		"Estructura del JSON para enviar en el cuerpo de la solicitud"
// @Success 201 {object} controllers.BodyRespuestaPostPaquetes
// @Failure 400 Error en los datos (retorna error explicativo) o estructura de la solicitud incorrecta
// @Failure 403 Token no válido
// @Failure 500 Error en el servidor
// @router / [post]
func (c *PAQUETESController) Post() {
	var v BodyPostPaquetes
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil {
		var usuarioProveedor models.USUARIOS
		tokenProv := v.TokenProv
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))
		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenProv).One(&usuarioProveedor)

		// Si el token no está en la base de datos o no pertenece a un proveedor
		if errorTokenNoEncontrado != nil || !o.QueryTable(new(models.PROVEEDORES)).Filter("ID_PROVEEDOR", usuarioProveedor.Id).Exist() {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.Data["json"] = "Token no válido"
			c.ServeJSON()
			return
		}
		// Crear objetos paquete y servicio de acuerdo a la solicitud realizada
		var paquete models.PAQUETES
		var servicios []models.SERVICIOS

		// Inicializar campos en el cuerpo de la solicitud
		paquete.CIUDADDESTINO = v.CiudadDestino
		paquete.COSTO = v.Precio
		paquete.DISPONIBILIDAD = v.CuposPublicados
		paquete.CUPOSPUBLICADOS = v.CuposPublicados

		for _, datosServicios := range v.Servicios {
			servicioIndividual := models.SERVICIOS{TITULOSERVICIO: datosServicios.Descripcion}
			if errorIntegridadServicio := models.RevisarIntegridadServicios(servicioIndividual, datosServicios.IdCategoria); errorIntegridadServicio != nil {
				// Error en los datos
				c.Ctx.Output.SetStatus(400)
				c.Data["json"] = errorIntegridadServicio.Error()
				c.ServeJSON()
				return
			}
			// Completar el objeto
			servicioIndividual.IDCATEGORIA, _ = models.GetCATEGORIASById(datosServicios.IdCategoria)
			servicios = append(servicios, servicioIndividual)
		}
		if errorIntegridadPaquete := models.RevisarIntegridadPaquetes(paquete); errorIntegridadPaquete != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(400)
			c.Data["json"] = errorIntegridadPaquete.Error()
			c.ServeJSON()
			return
		}
		// Insertar registro del paquete
		if id_paquete, error_insercion_paquete := models.AddPAQUETES(&paquete); error_insercion_paquete == nil {
			// Asignar mismo id al paquete
			idPaquete := int(id_paquete)
			// Insertar registro de los servicios
			for _, Servicios := range servicios {
				Servicios.IDPAQUETE, _ = models.GetPAQUETESById(idPaquete)
				if _, error_insercion_servicios := models.AddSERVICIOS(&Servicios); error_insercion_servicios != nil {
					// Eliminar registro del servicio
					models.DeleteSERVICIOS(Servicios.Id)
					c.Ctx.Output.SetStatus(500)
					c.Data["json"] = "Error en el servidor"
				}
			}
			// Se inserto todo, luego la creacion ha sido exitosa
			c.Ctx.Output.SetStatus(201)
			c.Data["json"] = BodyRespuestaPostPaquetes{Id: int(id_paquete)}
		} else {
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = err.Error()
		}
		c.ServeJSON()
	}
}

// GetOne ...
// @Title Get One
// @Description Obtener el registro de un paquete usando su :id (usar el header "Authorization" así "Bearer Token"). El token debe ser de un usuario o servidor registrado.
// @Param	id		path 	string	true		"El :id del registro del paquete cuya información se quiere obtener"
// @Success 200 {object} controllers.BodyRespuestaGetOnePaquetes
// @Failure 400 El parámetro :id no es un entero o la estructura del cuerpo de la solicitud no es correcta
// @Failure 403 Token no está en la base de datos
// @Failure 404 No existe registro con ese :id
// @router /:id [get]
func (c *PAQUETESController) GetOne() {
	// Obtener :id
	idStr := c.Ctx.Input.Param(":id")
	id, errID := strconv.Atoi(idStr)
	// Revisar si hubo un error al convertir el :id
	if errID != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "El parámetro :id no es un entero"
		c.ServeJSON()
		return
	}

	// Revisar estructura del encabezado de autorización
	valorEncabezado := strings.Split(strings.TrimPrefix(c.Ctx.Input.Header("Authorization"), " "), " ")
	if len(valorEncabezado) != 2 {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Valor del encabezado de autorización está mal formado"
		c.ServeJSON()
		return
	}
	token := valorEncabezado[1]

	o := orm.NewOrm()
	qs := o.QueryTable(new(models.USUARIOS))
	// Si el token no está en la base de datos
	if !qs.Filter("API_TOKEN", token).Exist() {
		// El solicitante no está autorizado para realizar la operación
		c.Ctx.Output.SetStatus(403)
		c.Data["json"] = "Token no está en la base de datos"
		c.ServeJSON()
		return
	}
	datosPaquete, errorIDNoExiste := models.GetPAQUETESById(id)
	if errorIDNoExiste != nil {
		// El id enviado no existe
		c.Ctx.Output.SetStatus(404)
		c.Data["json"] = "Recurso no existe"
	} else {
		// Lectura exitosa
		// Obtener los datos del proveedor
		usuarioProveedor := models.USUARIOS{Id: datosPaquete.IDPROVEEDOR.Id}
		o.Read(&usuarioProveedor)
		var servicios []*models.SERVICIOS
		_, err := o.QueryTable(new(models.SERVICIOS)).Filter("ID_PAQUETE", datosPaquete.Id).RelatedSel().All(&servicios)
		var datosServicios []BodyRespuestaGetOneServicios
		if err == nil {
			for _, servicio := range servicios {
				datosServicios = append(datosServicios, BodyRespuestaGetOneServicios{Categoria: BodyRespuestaGetOneCategorias{Id: servicio.IDCATEGORIA.Id, TituloCategoria: servicio.IDCATEGORIA.TITULOCATEGORIA},
					Descripcion: servicio.TITULOSERVICIO})
			}
		}
		c.Ctx.Output.SetStatus(200)
		c.Data["json"] = BodyRespuestaGetOnePaquetes{CiudadDestino: datosPaquete.CIUDADDESTINO,
			Precio: datosPaquete.COSTO, Proveedor: BodyRespuestaGetOneProveedoresParaAll{Apellidos: usuarioProveedor.APELLIDOS,
				Nombres: usuarioProveedor.NOMBRES, Correo: usuarioProveedor.CORREO}, CuposDisponibles: datosPaquete.DISPONIBILIDAD,
			CuposPublicados: datosPaquete.CUPOSPUBLICADOS, Servicios: BodyRespuestaGetAllServicios{servicios: datosServicios}, Id: datosPaquete.Id}
	}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description Obtener todos los registros de paquetes en la base de datos (usar el header "Authorization" así "Bearer Token"). El token debe ser de un usuario o servidor registrado.
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} controllers.BodyRespuestaGetAllPaquetes
// @Failure 400 Error en la estructura del body de la solicitud enviada o en la query string
// @Failure 403 Token no está en la base de datos
// @router / [get]
func (c *PAQUETESController) GetAll() {

	// Revisar estructura del encabezado de autorización
	valorEncabezado := strings.Split(strings.TrimPrefix(c.Ctx.Input.Header("Authorization"), " "), " ")
	if len(valorEncabezado) != 2 {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = "Valor del encabezado de autorización está mal formado"
		c.ServeJSON()
		return
	}
	token := valorEncabezado[1]
	o := orm.NewOrm()
	qs := o.QueryTable(new(models.USUARIOS))

	// Si el token no está en la base de datos
	if !qs.Filter("API_TOKEN", token).Exist() {
		// El solicitante no está autorizado para realizar la operación
		c.Ctx.Output.SetStatus(403)
		c.Data["json"] = "Token no está en la base de datos"
		c.ServeJSON()
		return
	}

	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllPAQUETES(query, fields, sortby, order, offset, limit)
	if err != nil {
		// La mayoría de los errores tienen que ver con los valores de la query string
		// Hay uno que no se ha podido identificar qué significa (cuándo ocurre). Ocurre en la línea:
		// _, err = qs.Limit(limit, offset).All(&l, fields...)
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = err.Error()
		c.ServeJSON()
		return
	} else {
		o := orm.NewOrm()
		// Crear slice que contendrá los registros a retornar
		var paquetes []BodyRespuestaGetOnePaquetes

		for _, paquete := range l {
			datosPaquete := paquete.(models.PAQUETES)
			// Obtener los datos del proveedor
			usuarioProveedor := models.USUARIOS{Id: datosPaquete.IDPROVEEDOR.Id}
			o.Read(&usuarioProveedor)
			// Obtener los datos del servicio
			var servicios []*models.SERVICIOS
			_, err := o.QueryTable(new(models.SERVICIOS)).Filter("ID_PAQUETE", datosPaquete.Id).RelatedSel().All(&servicios)
			var datosServicios []BodyRespuestaGetOneServicios
			if err == nil {
				for _, servicio := range servicios {
					datosServicios = append(datosServicios, BodyRespuestaGetOneServicios{Categoria: BodyRespuestaGetOneCategorias{Id: servicio.IDCATEGORIA.Id, TituloCategoria: servicio.IDCATEGORIA.TITULOCATEGORIA},
						Descripcion: servicio.TITULOSERVICIO})
				}
			}
			// Guardar los datos del paquete
			paquetes = append(paquetes,
				BodyRespuestaGetOnePaquetes{CiudadDestino: datosPaquete.CIUDADDESTINO,
					Precio: datosPaquete.COSTO, Proveedor: BodyRespuestaGetOneProveedoresParaAll{Apellidos: usuarioProveedor.APELLIDOS,
						Nombres: usuarioProveedor.NOMBRES, Correo: usuarioProveedor.CORREO}, CuposDisponibles: datosPaquete.DISPONIBILIDAD,
					CuposPublicados: datosPaquete.CUPOSPUBLICADOS, Servicios: BodyRespuestaGetAllServicios{servicios: datosServicios}, Id: datosPaquete.Id})
		}

		// Retornar struct correspondiente inicializándola con el slice
		c.Data["json"] = BodyRespuestaGetAllPaquetes{Paquetes: paquetes}
	}
	// Solicitud exitosa
	c.Ctx.Output.SetStatus(200)
	c.ServeJSON()
}

// Put ...
// @Title Put
// @Description Modificar el registro de un paquete identificado por su id
// @Param	id		path 	string	true		"El id del paquete que se quiere actualizar"
// @Param	body		body 	controllers.BodyPutPaquetes	true		"Campos a actualizar"
// @Success 200 La modificación fue exitosa
// @Failure 400 La solicitud no tiene la estructura correcta o el :id no es un entero.
// @Failure 403 Solicitante no está autorizado para actualizar recurso.
// @Failure 409 Los datos enviados no cumplen con los requisitos de integridad (se retorna una cadena con el error).
// @Failure 500 Solicitud no se pudo llevar a cabo por un error interno en el servidor.
// @router /:id [put]
func (c *PAQUETESController) Put() {
	idStr := c.Ctx.Input.Param(":id")
	id, errorIndice := strconv.Atoi(idStr)
	var v BodyPutPaquetes
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil && errorIndice == nil {
		// Crear objetos paquete y servicio de acuerdo a la solicitud realizada
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))
		paquete := models.PAQUETES{Id: id}
		o.Read(&paquete)
		var servicios []models.SERVICIOS
		var usuarioProveedor models.USUARIOS
		tokenProv := v.TokenProv
		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenProv).One(&usuarioProveedor)
		// Si el token no está en la base de datos o no pertenece a un proveedor
		if errorTokenNoEncontrado != nil || !o.QueryTable(new(models.PROVEEDORES)).Filter("ID_PROVEEDOR", usuarioProveedor.Id).Exist() ||
			paquete.IDPROVEEDOR.Id != usuarioProveedor.Id {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.ServeJSON()
			return
		}
		// Inicializar campos en el cuerpo de la solicitud
		paquete.CIUDADDESTINO = v.CiudadDestino
		paquete.COSTO = v.Precio
		cambioCuposDisponibles := v.CuposDisponibles - paquete.DISPONIBILIDAD
		paquete.DISPONIBILIDAD = v.CuposDisponibles
		// Cupos publicados = cupos disponibles + cupos vendidos
		paquete.CUPOSPUBLICADOS += cambioCuposDisponibles

		for _, datosServicios := range v.Servicios {
			servicio := models.SERVICIOS{Id: datosServicios.IdServicio}
			o.Read(&servicio)
			servicio.TITULOSERVICIO = datosServicios.Descripcion
			servicio.IDCATEGORIA, _ = models.GetCATEGORIASById(datosServicios.IdCategoria)
			if errorIntegridadServicio := models.RevisarIntegridadServicios(servicio, datosServicios.IdCategoria); errorIntegridadServicio != nil {
				// Error en los datos
				c.Ctx.Output.SetStatus(400)
				c.Data["json"] = errorIntegridadServicio.Error()
				c.ServeJSON()
				return
			}
			// Completar el objeto
			servicios = append(servicios, servicio)
		}
		if errorIntegridadPaquete := models.RevisarIntegridadPaquetes(paquete); errorIntegridadPaquete != nil {
			// Error en los datos
			c.Ctx.Output.SetStatus(400)
			c.Data["json"] = errorIntegridadPaquete.Error()
			c.ServeJSON()
			return
		}
		if error_update_paquete := models.UpdatePAQUETESById(&paquete); error_update_paquete == nil {
			// Insertar registro de los servicios
			for _, Servicios := range servicios {
				if error_update_servicios := models.UpdateSERVICIOSById(&Servicios); error_update_servicios != nil {
					c.Ctx.Output.SetStatus(500)
					c.Data["json"] = "Error en el servidor"
				}
			}
			// Se inserto todo, luego la creacion ha sido exitosa
			c.Ctx.Output.SetStatus(200)
		} else {
			c.Ctx.Output.SetStatus(500)
		}
	} else {
		c.Ctx.Output.SetStatus(400)
	}
	c.ServeJSON()
}

// Delete ...
// @Title Delete
// @Description delete the PAQUETES
// @Param	id		path 	string	true		"El id del paquete que se quiere eliminar"
// @Param	body		body 	controllers.BodyDeletePaquetes	true		"Estructura JSON para enviar en el cuerpo de la solicitud"
// @Success 200 La eliminación fue exitosa
// @Failure 400 La solicitud no tiene la estructura correcta o el :id no es un entero.
// @Failure 403 Solicitante no está autorizado para actualizar recurso.
// @Failure 500 Solicitud no se pudo llevar a cabo por un error interno en el servidor.
// @router /:id [delete]
func (c *PAQUETESController) Delete() {
	var v BodyDeletePaquetes
	idStr := c.Ctx.Input.Param(":id")
	id, errorIndice := strconv.Atoi(idStr)
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &v); err == nil && errorIndice == nil {
		// Crear objetos paquete y servicio de acuerdo a la solicitud realizada
		o := orm.NewOrm()
		qs := o.QueryTable(new(models.USUARIOS))
		if o.QueryTable(new(models.PAQUETES)).Filter("ID_PAQUETE", id).Exist() {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.ServeJSON()
			return
		}
		paquete := models.PAQUETES{Id: id}
		o.Read(&paquete)
		var usuarioProveedor models.USUARIOS
		tokenProv := v.TokenProv
		errorTokenNoEncontrado := qs.Filter("API_TOKEN", tokenProv).One(&usuarioProveedor)
		// Si el token no está en la base de datos o no pertenece a un proveedor
		if errorTokenNoEncontrado != nil || !o.QueryTable(new(models.PROVEEDORES)).Filter("ID_PROVEEDOR", usuarioProveedor.Id).Exist() || paquete.IDPROVEEDOR.Id != usuarioProveedor.Id {
			// Error en la autenticación de la petición
			c.Ctx.Output.SetStatus(403)
			c.ServeJSON()
			return
		}

		// Obtener los servicios asociados al paquete
		var servicios []models.SERVICIOS
		_, err := o.QueryTable("SERVICIOS").Filter("ID_PAQUETE", id).RelatedSel().All(&servicios)
		if err == nil {
			for _, datosServicios := range servicios {
				if err := models.DeleteSERVICIOS(datosServicios.Id); err != nil {
					c.Ctx.Output.SetStatus(500)
					return
				}
			}
			c.Ctx.Output.SetStatus(200)
		}
		if err := models.DeletePAQUETES(id); err == nil {
			c.Ctx.Output.SetStatus(200)
		} else {
			c.Ctx.Output.SetStatus(500)
		}
	} else {
		c.Ctx.Output.SetStatus(400)
	}
	c.ServeJSON()
}
