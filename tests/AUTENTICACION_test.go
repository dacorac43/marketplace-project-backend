package tests

import (
	"backendmarketplace/controllers"
	"backendmarketplace/models"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"testing"

	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

// TestAUTENTICACIONPost Función de prueba unitaria para la función controllers.AUTENTICACION.Post
func TestAUTENTICACIONPost(t *testing.T) {

	// Creación de los recursos necesarios
	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()
	// - Ingresar un administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin', 'admin', 'admin@correo.co', ?, NULL)", models.CalcularHashPassword("1234567Aa")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()
	// - Ingresar dos clientes a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'cliente1', 'cliente1', 'cli1@correo.co', ?, NULL)", models.CalcularHashPassword("1234567Aa")).Exec()
	o.Raw("INSERT INTO CLIENTES (ID_USUARIO, ID_ADMIN_VALIDACION) VALUES (2, 1)").Exec()
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (3, 'cliente2', 'cliente2', 'cli2@correo.co', ?, NULL)", models.CalcularHashPassword("1234567Aa")).Exec()
	o.Raw("INSERT INTO CLIENTES (ID_USUARIO, ID_ADMIN_VALIDACION) VALUES (3, 1)").Exec()
	// - Ingresar un proveedor a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (4, 'prove1', 'prove1', 'prove@correo.co', ?, NULL)", models.CalcularHashPassword("1234567Aa")).Exec()
	o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO) VALUES (4, 1)").Exec()

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso      string
		datosSolicitud  controllers.BODY_POST_TOKEN
		statusEsperado  int
		errorEsperado   string
		camposCorrectos func(respuestaObtenida controllers.BODY_RESPUESTA_POST_TOKEN) bool
	}{
		{
			nombreCaso:     "Test 1: Autenticación exitosa - Administradores",
			datosSolicitud: controllers.BODY_POST_TOKEN{Correo: "admin@correo.co", Password: "1234567Aa"},
			statusEsperado: http.StatusCreated,
			camposCorrectos: func(respuestaObtenida controllers.BODY_RESPUESTA_POST_TOKEN) bool {
				administrador := &models.USUARIOS{Id: 1}
				if errorNoEncontrado := o.Read(administrador); errorNoEncontrado != nil {
					return false
				}
				return respuestaObtenida.Apellido == "admin" && respuestaObtenida.Nombre == "admin" &&
					respuestaObtenida.Correo == "admin@correo.co" && respuestaObtenida.Rol == 1 &&
					respuestaObtenida.Token == administrador.APITOKEN && respuestaObtenida.Id == 1
			},
		},
		{
			nombreCaso:     "Test 2: Autenticación exitosa - Clientes",
			datosSolicitud: controllers.BODY_POST_TOKEN{Correo: "cli1@correo.co", Password: "1234567Aa"},
			statusEsperado: http.StatusCreated,
			camposCorrectos: func(respuestaObtenida controllers.BODY_RESPUESTA_POST_TOKEN) bool {
				cliente := &models.USUARIOS{Id: 2}
				if errorNoEncontrado := o.Read(cliente); errorNoEncontrado != nil {
					return false
				}
				return respuestaObtenida.Apellido == "cliente1" && respuestaObtenida.Nombre == "cliente1" &&
					respuestaObtenida.Correo == "cli1@correo.co" && respuestaObtenida.Rol == 3 &&
					respuestaObtenida.Token == cliente.APITOKEN && respuestaObtenida.Id == 2
			},
		},
		{
			nombreCaso:     "Test 3: Autenticación exitosa - Proveedores",
			datosSolicitud: controllers.BODY_POST_TOKEN{Correo: "prove@correo.co", Password: "1234567Aa"},
			statusEsperado: http.StatusCreated,
			camposCorrectos: func(respuestaObtenida controllers.BODY_RESPUESTA_POST_TOKEN) bool {
				proveedor := &models.USUARIOS{Id: 4}
				if errorNoEncontrado := o.Read(proveedor); errorNoEncontrado != nil {
					return false
				}
				return respuestaObtenida.Apellido == "prove1" && respuestaObtenida.Nombre == "prove1" &&
					respuestaObtenida.Correo == "prove@correo.co" && respuestaObtenida.Rol == 2 &&
					respuestaObtenida.Token == proveedor.APITOKEN && respuestaObtenida.Id == 4
			},
		},
		{
			nombreCaso:     "Test 4: Usuario no registrado",
			datosSolicitud: controllers.BODY_POST_TOKEN{Correo: "nor@correo.co", Password: "pass6"},
			statusEsperado: http.StatusUnauthorized,
			errorEsperado:  "\"Autenticación fallida\"",
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Crear solicitud
			cuerpoSolicitud := new(bytes.Buffer)
			json.NewEncoder(cuerpoSolicitud).Encode(casoDePrueba.datosSolicitud)
			respuestaObtenida, error := http.Post("http://localhost:8080/v1/TOKEN/", "application/json", cuerpoSolicitud)

			if error != nil {
				t.Error(error.Error())
			}

			// Siempre se debe cerrar el body de la respuesta luego de usarlo
			defer respuestaObtenida.Body.Close()

			// Revisar código de estado
			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
			}

			// Obtener respuesta
			var cuerpoRespuestaObtenida controllers.BODY_RESPUESTA_POST_TOKEN
			// Obtener response body como slice de bytes
			cuerpoRespuestaObtenidaBytes, _ := ioutil.ReadAll(respuestaObtenida.Body)
			// Usar el slice de bytes para intentar obtener un struct de respuesta exitosa
			error = json.Unmarshal(cuerpoRespuestaObtenidaBytes, &cuerpoRespuestaObtenida)

			// Si el cuerpo de la respuesta obtenida tiene un error
			if error != nil {
				errorRespuestaObtenida := string(cuerpoRespuestaObtenidaBytes)
				// - Si no se esperaba un error
				if casoDePrueba.errorEsperado == "" {
					t.Errorf("Error: no se esperaba un error. Retornó: %s", errorRespuestaObtenida)
				} else if errorRespuestaObtenida != casoDePrueba.errorEsperado {
					// - Si los errores esperado y obtenido son diferentes
					t.Errorf("Error: se obtuvo %s. Se esperaba %s", errorRespuestaObtenida, casoDePrueba.errorEsperado)
				}
			} else {
				// Si el cuerpo de la respuesta obtenida NO es un mensaje de error

				// - Si se esperaba un error
				if casoDePrueba.errorEsperado != "" {
					t.Errorf("Error: se esperaba un error. Se obtuvo %+v", cuerpoRespuestaObtenida)
				} else if !casoDePrueba.camposCorrectos(cuerpoRespuestaObtenida) {
					// - Si los valores de la respuesta son diferentes a los esperados
					t.Errorf("Error: Los campos no son correctos. Se obtuvo %+v", cuerpoRespuestaObtenida)
				}
			}

		})
	}
	// Liberación de recursos
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

// TestAutenticacionDelete Función de prueba unitaria para la función controllers.AUTENTICACION.Delete
func TestAUTENTICACIONDelete(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	// - Obtener token del administrador creado
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)
	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso              string
		querySolicitud          string
		cuerpoSolicitud         controllers.BODY_DELETE_TOKEN
		statusEsperado          int
		cuerpoRespuestaEsperada *controllers.BODY_RESPUESTA_POST_TOKEN
		errorEsperado           string
	}{
		{
			nombreCaso:      "Test 1: Token eliminado",
			querySolicitud:  "",
			statusEsperado:  http.StatusOK,
			cuerpoSolicitud: controllers.BODY_DELETE_TOKEN{Token: administrador.APITOKEN},
		},
		{
			nombreCaso:      "Test 2: Token invalido",
			querySolicitud:  "",
			cuerpoSolicitud: controllers.BODY_DELETE_TOKEN{Token: "e83e8535d6f689493e5819bd60aa3e5fdcba940e6d111ab6fb5c34f24f86496bf3726e2bf4ec59d6d2f5a2aeb1e4f103283e7d64e4f49c03b4c4725cb361e773"},
			statusEsperado:  http.StatusForbidden,
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			//Crear solicitud
			//cuerpo, err := json.Marshal(casoDePrueba.cuerpoSolicitud)
			//if err != nil {
			//	log.Fatalf("json.Marshal() tuvo un fallo: '%s'\n", err)
			//}
			//body := bytes.NewBuffer(cuerpo)
			client := &http.Client{}
			cuerpoToken := new(bytes.Buffer)
			json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, error := http.NewRequest("DELETE", "http://localhost:8080/v1/TOKEN/"+casoDePrueba.querySolicitud, cuerpoToken)
			solicitudCreada.Header.Set("Content-Type", "application/json; charset=utf-8")

			// Enviar solicitud
			resp, err := client.Do(solicitudCreada)
			if err != nil {
				log.Fatalf("client.Do() failed with '%s'\n", err)
			}

			defer resp.Body.Close()

			if error != nil {
				t.Error(error.Error())
				return
			}

			if resp.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", resp.StatusCode, casoDePrueba.statusEsperado)
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registros de administradores
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}
