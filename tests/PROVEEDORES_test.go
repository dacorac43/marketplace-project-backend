package tests

import (
	"backendmarketplace/controllers"
	"backendmarketplace/models"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"
	"testing"

	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

// TestProveedoresPost Función de prueba unitaria para la función controllers.PROVEEDORES.Post
func TestPROVEEDORESPost(t *testing.T) {
	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar el administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'adm', '', 'adm@correo.co', ?, NULL)", models.CalcularHashPassword("pa1")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	// - Obtener token del administrador creado
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso              string
		datosSolicitud          controllers.BODY_POST_PROVEEDORES
		statusEsperado          int
		cuerpoRespuestaEsperada *controllers.BODY_RESPUESTA_POST_PROVEEDORES
		errorEsperado           string
		antes                   func()
		despues                 func()
	}{
		{
			nombreCaso:              "Test 1: creación exitosa",
			datosSolicitud:          controllers.BODY_POST_PROVEEDORES{Apellidos: "Caro Lopez", Correo: "lc@correo.co", Password: "password1A", Nombres: "Luis Alfredo", Organizacion: "Organ1", Token: administrador.APITOKEN},
			statusEsperado:          http.StatusCreated,
			cuerpoRespuestaEsperada: &controllers.BODY_RESPUESTA_POST_PROVEEDORES{Id: 2},
			despues: func() {
				// Eliminar registro usando el correo (es suficiente porque es único)
				var usuarioAEliminar models.USUARIOS
				qs := o.QueryTable(new(models.USUARIOS))
				error := qs.Filter("CORREO", "lc@correo.co").One(&usuarioAEliminar)
				// Si se pudo realizar la creación del proveedor
				if error == nil {
					proveedorAEliminar := models.PROVEEDORES{Id: usuarioAEliminar.Id}
					o.Delete(&proveedorAEliminar)
					o.Delete(&usuarioAEliminar)
				}
			},
		},

		{
			nombreCaso:     "Test 2: correo ya registrado",
			datosSolicitud: controllers.BODY_POST_PROVEEDORES{Apellidos: "Moreno", Correo: "lc@correo.co", Password: "pass2", Nombres: "Carlos", Organizacion: "Sprite", Token: administrador.APITOKEN},
			statusEsperado: http.StatusBadRequest,
			errorEsperado:  "\"Correo ya registrado\"",
			antes: func() {
				// Ingresar registro con el mismo correo
				o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
					"VALUES (2, 'Carlos', 'Moreno', 'lc@correo.co', ?, NULL)", models.CalcularHashPassword("pa2")).Exec()
				o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO, ORGANIZACION) VALUES (2, 1, 'Sprite')").Exec()
			},
			despues: func() {
				// Eliminar registros ingresados en antes()
				o.Raw("DELETE FROM PROVEEDORES WHERE ID_PROVEEDOR = 2").Exec()
				o.Raw("DELETE FROM USUARIOS WHERE ID_USUARIO = 2").Exec()
			},
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Preparar ambiente para caso de prueba
			if casoDePrueba.antes != nil {
				casoDePrueba.antes()
			}
			// Cuando la función retorne dejar ambiente como estaba
			if casoDePrueba.despues != nil {
				defer casoDePrueba.despues()
			}
			// Crear solicitud
			cuerpoSolicitud := new(bytes.Buffer)
			json.NewEncoder(cuerpoSolicitud).Encode(casoDePrueba.datosSolicitud)
			respuestaObtenida, error := http.Post("http://localhost:8080/v1/PROVEEDORES", "application/json", cuerpoSolicitud)

			if error != nil {
				t.Error(error.Error())
			}

			// Siempre se debe cerrar el body de la respuesta luego de usarlo
			defer respuestaObtenida.Body.Close()

			// Revisar código de estado
			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
			}

			// Obtener respuesta
			var cuerpoRespuestaObtenida controllers.BODY_RESPUESTA_POST_PROVEEDORES
			// Obtener response body como slice de bytes
			cuerpoRespuestaObtenidaBytes, _ := ioutil.ReadAll(respuestaObtenida.Body)
			// Usar el slice de bytes para intentar obtener un struct de respuesta exitosa
			error = json.Unmarshal(cuerpoRespuestaObtenidaBytes, &cuerpoRespuestaObtenida)

			// Si el cuerpo de la respuesta obtenida tiene un error
			if error != nil {
				errorRespuestaObtenida := string(cuerpoRespuestaObtenidaBytes)
				// - Si no se esperaba un error
				if casoDePrueba.errorEsperado == "" {
					t.Errorf("Error: no se esperaba un error. Retornó: %s", errorRespuestaObtenida)
				} else if errorRespuestaObtenida != casoDePrueba.errorEsperado {
					// - Si los errores esperado y obtenido son diferentes
					t.Errorf("Error: se obtuvo %s. Se esperaba %s", errorRespuestaObtenida, casoDePrueba.errorEsperado)
				}
			} else {
				// Si el cuerpo de la respuesta obtenida NO es un mensaje de error

				// - Si se esperaba un error
				if casoDePrueba.errorEsperado != "" {
					t.Errorf("Error: se esperaba un error. Se obtuvo %+v", cuerpoRespuestaObtenida)
				} else if *(casoDePrueba.cuerpoRespuestaEsperada) != cuerpoRespuestaObtenida {
					// - Si los body de las respuesta obtenida y esperada son diferentes
					t.Errorf("Error: el cuerpo retornado: %+v, no es el esperado: %+v", cuerpoRespuestaObtenida, *casoDePrueba.cuerpoRespuestaEsperada)
				}
			}

		})
	}
	// Liberación de recursos

	// - Eliminar registro del administrador
	o.Raw("DELETE FROM PROVEEDORES WHERE ID_ADMIN = ?", administrador.Id).Exec()
	o.Delete(&administrador)
}

// TestProveedoresGetOneFunción de prueba unitaria para la función controllers.PROVEEDORES.GetOne
func TestPROVEEDORESGetOne(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un usuario de cada rol (administradores, clientes y proveedores) a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'prov1', '', 'prov1@correo.co', ?, NULL)", models.CalcularHashPassword("1234")).Exec()
	o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO, ID_ADMIN_ELIMINACION, ORGANIZACION) VALUES (2, 1, NULL, 'Unal')").Exec()
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (3, 'cli1', '', 'cli1@correo.co', ?, NULL)", models.CalcularHashPassword("12345")).Exec()
	o.Raw("INSERT INTO CLIENTES (ID_USUARIO, ID_ADMIN_VALIDACION) VALUES (3, 1)").Exec()

	// - Generar tokens
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)

	proveedor := models.USUARIOS{Id: 2}
	o.Read(&proveedor)
	proveedor.APITOKEN = models.CalcularApiToken(proveedor)
	o.Update(&proveedor)

	cliente := models.USUARIOS{Id: 3}
	o.Read(&cliente)
	cliente.APITOKEN = models.CalcularApiToken(cliente)
	o.Update(&cliente)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso              string
		statusEsperado          int
		cuerpoRespuestaEsperada *controllers.BodyRespuestaGetOneProveedores
		errorEsperado           string
		//cuerpoSolicitud         controllers.BodyGetOneProveedores
		token          string
		querySolicitud string
	}{
		{
			nombreCaso:     "Test 1: :id no es un entero",
			statusEsperado: http.StatusBadRequest,
			errorEsperado:  "\"El parámetro :id no es un entero\"",
			//cuerpoSolicitud: controllers.BodyGetOneProveedores{TokenUsuario: administrador.APITOKEN},
			token:          administrador.APITOKEN,
			querySolicitud: "a",
		},
		{
			nombreCaso:     "Test 2: el Token no está en la base de datos",
			statusEsperado: http.StatusForbidden,
			errorEsperado:  "\"Token no está en la base de datos\"",
			//cuerpoSolicitud: controllers.BodyGetOneProveedores{TokenUsuario: "TokenQueNoExisteEnBD"},
			token:          "TokenQueNoExiste",
			querySolicitud: "3",
		},
		{
			nombreCaso:     "Test 3: Solicitud de un recurso que no existe",
			statusEsperado: http.StatusNotFound,
			errorEsperado:  "\"Recurso no existe\"",
			//cuerpoSolicitud: controllers.BodyGetOneProveedores{TokenUsuario: proveedor.APITOKEN},
			token:          cliente.APITOKEN,
			querySolicitud: "100",
		},
		{
			nombreCaso:     "Test 4: lectura exitosa solicitada por un administrador",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetOneProveedores{
				Correo: "prov1@correo.co", Nombres: "prov1",
			},
			//cuerpoSolicitud: controllers.BodyGetOneProveedores{TokenUsuario: administrador.APITOKEN},
			token:          administrador.APITOKEN,
			querySolicitud: "2",
		},
		{
			nombreCaso:     "Test 5: lectura exitosa solicitada por un proveedor",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetOneProveedores{
				Correo: "prov1@correo.co", Nombres: "prov1",
			},
			//cuerpoSolicitud: controllers.BodyGetOneProveedores{TokenUsuario: proveedor.APITOKEN},
			token:          proveedor.APITOKEN,
			querySolicitud: "2",
		},
		{
			nombreCaso:     "Test 6: lectura exitosa solicitada por un cliente",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetOneProveedores{
				Correo: "prov1@correo.co", Nombres: "prov1",
			},
			//cuerpoSolicitud: controllers.BodyGetOneProveedores{TokenUsuario: cliente.APITOKEN},
			token:          cliente.APITOKEN,
			querySolicitud: "2",
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			cliente := &http.Client{}
			//cuerpoToken := new(bytes.Buffer)
			//json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, errorCreacionSolicitud := http.NewRequest("GET", "http://localhost:8080/v1/PROVEEDORES/"+casoDePrueba.querySolicitud, nil)
			solicitudCreada.Header.Set("Authorization", "Bearer "+casoDePrueba.token)
			if errorCreacionSolicitud != nil {
				t.Errorf("Error al crear la solicitud: %s", errorCreacionSolicitud)
				return
			}

			// Enviar solicitud
			respuestaObtenida, errorEnvioSolicitud := cliente.Do(solicitudCreada)
			if errorEnvioSolicitud != nil {
				t.Errorf("Error al enviar la solicitud: %s", errorEnvioSolicitud)
				return
			}

			defer respuestaObtenida.Body.Close()

			// Revisar código de estado
			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
				return
			}

			// Obtener respuesta
			var cuerpoRespuestaObtenida controllers.BodyRespuestaGetOneProveedores
			// Obtener response body como slice de bytes
			cuerpoRespuestaObtenidaBytes, _ := ioutil.ReadAll(respuestaObtenida.Body)
			// Usar el slice de bytes para intentar obtener un struct de respuesta exitosa
			error := json.Unmarshal(cuerpoRespuestaObtenidaBytes, &cuerpoRespuestaObtenida)

			// Si el cuerpo de la respuesta indica que hubo un error
			if error != nil {
				errorRespuestaObtenida := string(cuerpoRespuestaObtenidaBytes)
				// - Si no se esperaba un error
				if casoDePrueba.errorEsperado == "" {
					t.Errorf("Error: no se esperaba un error. Retornó: %s", errorRespuestaObtenida)
				} else if errorRespuestaObtenida != casoDePrueba.errorEsperado {
					// - Si los errores esperado y obtenido son diferentes
					t.Errorf("Error: se obtuvo %s. Se esperaba %s", errorRespuestaObtenida, casoDePrueba.errorEsperado)
				}
			} else {
				// Si el cuerpo de la respuesta obtenida NO es un mensaje de error

				// - Si se esperaba un error
				if casoDePrueba.errorEsperado != "" {
					t.Errorf("Error: se esperaba un error. Se obtuvo %+v", cuerpoRespuestaObtenida)
				} else if !reflect.DeepEqual(*(casoDePrueba.cuerpoRespuestaEsperada), cuerpoRespuestaObtenida) {
					// - Si los body de las respuesta obtenida y esperada son diferentes
					t.Errorf("Error: el cuerpo retornado: %+v, no es el esperado: %+v", cuerpoRespuestaObtenida, *casoDePrueba.cuerpoRespuestaEsperada)
				}
			}
		})
	}

	// Liberación de recursos

	// - Eliminar registros de administradores
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

// TestProveedoresGetAll Función de prueba unitaria para la función controllers.PROVEEDORES.GetAll
func TestPROVEEDORESGetAll(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	// - Ingresar proveedores a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'prove1', '', 'prov1@correo.co', ?, NULL)", models.CalcularHashPassword("pa2")).Exec()
	o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO, ORGANIZACION) VALUES (2, 1, '')").Exec()

	// - Ingresar un cliente a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (3, 'cli1', '', 'cli1@correo.co', ?, NULL)", models.CalcularHashPassword("12345")).Exec()
	o.Raw("INSERT INTO CLIENTES (ID_USUARIO, ID_ADMIN_VALIDACION) VALUES (3, 1)").Exec()

	// - Generar tokens
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)

	proveedor := models.USUARIOS{Id: 2}
	o.Read(&proveedor)
	proveedor.APITOKEN = models.CalcularApiToken(proveedor)
	o.Update(&proveedor)

	cliente := models.USUARIOS{Id: 3}
	o.Read(&cliente)
	cliente.APITOKEN = models.CalcularApiToken(cliente)
	o.Update(&cliente)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso              string
		querySolicitud          string
		statusEsperado          int
		cuerpoRespuestaEsperada *controllers.BodyRespuestaGetAllProveedores
		errorEsperado           string
		//cuerpoSolicitud         controllers.BodyGetAllProveedores
		token   string
		antes   func()
		despues func()
	}{
		{
			nombreCaso:     "Test 1: lectura exitosa solicitada por un administrador",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllProveedores{
				Proveedores: []controllers.BodyRespuestaGetOneProveedoresParaAll{
					{Correo: "prov1@correo.co", Nombres: "prove1", Id: 2},
				},
			},
			token: administrador.APITOKEN,
			//cuerpoSolicitud: controllers.BodyGetAllProveedores{TokenUsuario: administrador.APITOKEN},
		},
		{
			nombreCaso:     "Test 2: lectura exitosa solicitada por un proveedor",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllProveedores{
				Proveedores: []controllers.BodyRespuestaGetOneProveedoresParaAll{
					{Correo: "prov1@correo.co", Nombres: "prove1", Id: 2},
				},
			},
			token: proveedor.APITOKEN,
			//cuerpoSolicitud: controllers.BodyGetAllProveedores{TokenUsuario: proveedor.APITOKEN},
		},
		{
			nombreCaso:     "Test 3: lectura exitosa solicitada por un cliente",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllProveedores{
				Proveedores: []controllers.BodyRespuestaGetOneProveedoresParaAll{
					{Correo: "prov1@correo.co", Nombres: "prove1", Id: 2},
				},
			},
			token: cliente.APITOKEN,
			//cuerpoSolicitud: controllers.BodyGetAllProveedores{TokenUsuario: cliente.APITOKEN},
		},
		{
			nombreCaso:     "Test 4: lectura exitosa mas de 1 registro",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllProveedores{
				Proveedores: []controllers.BodyRespuestaGetOneProveedoresParaAll{
					{Correo: "prov1@correo.co", Nombres: "prove1", Id: 2},
					{Correo: "prov2@correo.co", Nombres: "prove2", Id: 4},
				},
			},
			token: proveedor.APITOKEN,
			//cuerpoSolicitud: controllers.BodyGetAllProveedores{TokenUsuario: proveedor.APITOKEN},
			antes: func() {
				// Agregar un nuevo registro de proveedor
				o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
					"VALUES (4, 'prove2', '', 'prov2@correo.co', ?, NULL)", models.CalcularHashPassword("pa4")).Exec()
				o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO, ORGANIZACION) VALUES (4, 1, '')").Exec()
			},
			despues: func() {
				// Eliminar nuevo registro de proveedor
				o.Raw("DELETE FROM PROVEEDORES WHERE ID_PROVEEDOR = 4").Exec()
				o.Raw("DELETE FROM USUARIOS WHERE ID_USUARIO = 4").Exec()
			},
		},
		{
			nombreCaso:              "Test 5: lectura exitosa 0 registros",
			statusEsperado:          http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllProveedores{},
			token:                   administrador.APITOKEN,
			//cuerpoSolicitud:         controllers.BodyGetAllProveedores{TokenUsuario: administrador.APITOKEN},
			antes: func() {
				// Eliminar registro de proveedor con id 2
				o.Raw("DELETE FROM PROVEEDORES WHERE ID_PROVEEDOR = 2").Exec()
				o.Raw("DELETE FROM USUARIOS WHERE ID_USUARIO = 2").Exec()
			},
			despues: func() {
				// Volver a agregar registro de proveedor con id 2
				o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
					"VALUES (2, 'prove1', '', 'prov1@correo.co', ?, NULL)", models.CalcularHashPassword("pa2")).Exec()
				o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO, ORGANIZACION) VALUES (2, 1, '')").Exec()
				// Recalcular token
				proveedor := models.USUARIOS{Id: 2}
				o.Read(&proveedor)
				proveedor.APITOKEN = models.CalcularApiToken(proveedor)
				o.Update(&proveedor)
			},
		},
		{
			nombreCaso:     "Test 6: Token no está en la base de datos",
			statusEsperado: http.StatusForbidden,
			errorEsperado:  "\"Token no está en la base de datos\"",
			token:          "TokenNoValido",
			//cuerpoSolicitud: controllers.BodyGetAllProveedores{TokenUsuario: "TokenNoValido"},
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {
			// Preparar ambiente para caso de prueba
			if casoDePrueba.antes != nil {
				casoDePrueba.antes()
			}
			// Cuando la función retorne dejar ambiente como estaba
			if casoDePrueba.despues != nil {
				defer casoDePrueba.despues()
			}

			cliente := &http.Client{}
			//cuerpoToken := new(bytes.Buffer)
			//json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, errorCreacionSolicitud := http.NewRequest("GET", "http://localhost:8080/v1/PROVEEDORES", nil)
			solicitudCreada.Header.Set("Authorization", "Bearer "+casoDePrueba.token)
			if errorCreacionSolicitud != nil {
				t.Errorf("Error al crear la solicitud: %s", errorCreacionSolicitud)
				return
			}

			// Enviar solicitud
			respuestaObtenida, errorEnvioSolicitud := cliente.Do(solicitudCreada)
			if errorEnvioSolicitud != nil {
				t.Errorf("Error al enviar la solicitud: %s", errorEnvioSolicitud)
				return
			}

			defer respuestaObtenida.Body.Close()

			// Revisar código de estado
			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
				return
			}

			// Obtener respuesta
			var cuerpoRespuestaObtenida controllers.BodyRespuestaGetAllProveedores
			// Obtener response body como slice de bytes
			cuerpoRespuestaObtenidaBytes, _ := ioutil.ReadAll(respuestaObtenida.Body)
			// Usar el slice de bytes para intentar obtener un struct de respuesta exitosa
			error := json.Unmarshal(cuerpoRespuestaObtenidaBytes, &cuerpoRespuestaObtenida)

			// Si el cuerpo de la respuesta indica que hubo un error
			if error != nil {
				errorRespuestaObtenida := string(cuerpoRespuestaObtenidaBytes)
				// - Si no se esperaba un error
				if casoDePrueba.errorEsperado == "" {
					t.Errorf("Error: no se esperaba un error. Retornó: %s", errorRespuestaObtenida)
				} else if errorRespuestaObtenida != casoDePrueba.errorEsperado {
					// - Si los errores esperado y obtenido son diferentes
					t.Errorf("Error: se obtuvo %s. Se esperaba %s", errorRespuestaObtenida, casoDePrueba.errorEsperado)
				}
			} else {
				// Si el cuerpo de la respuesta obtenida NO es un mensaje de error

				// - Si se esperaba un error
				if casoDePrueba.errorEsperado != "" {
					t.Errorf("Error: se esperaba un error. Se obtuvo %+v", cuerpoRespuestaObtenida)
				} else if !reflect.DeepEqual(*(casoDePrueba.cuerpoRespuestaEsperada), cuerpoRespuestaObtenida) {
					// - Si los body de las respuesta obtenida y esperada son diferentes
					t.Errorf("Error: el cuerpo retornado: %+v, no es el esperado: %+v", cuerpoRespuestaObtenida, *casoDePrueba.cuerpoRespuestaEsperada)
				}
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registros de administradores
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

// TestProveedoresPut Función de prueba unitaria para la función controllers.PROVEEDOR.Put
func TestPROVEEDORESPut(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	// - Ingresar proveedores a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'prove1', '', 'prov1@correo.co', ?, NULL)", models.CalcularHashPassword("pa2")).Exec()
	o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO, ORGANIZACION) VALUES (2, 1, '')").Exec()

	// - Obtener token del administrador creado
	proveedor := models.USUARIOS{Id: 2}
	o.Read(&proveedor)
	proveedor.APITOKEN = models.CalcularApiToken(proveedor)
	o.Update(&proveedor)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso      string
		querySolicitud  string
		cuerpoSolicitud controllers.BodyPutProveedores
		statusEsperado  int
		errorEsperado   string
	}{
		{
			nombreCaso:      "Test 1: Moficación exitosa",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyPutProveedores{Apellidos: "Caro Lopez", Correo: "lc@correo.co", Password: "password1A", Nombres: "Luis Alfredo", Organizacion: "Organ1", Token: proveedor.APITOKEN},
			statusEsperado:  http.StatusOK,
		},
		{
			nombreCaso:      "Test 2: Moficación fallida",
			querySolicitud:  "3",
			cuerpoSolicitud: controllers.BodyPutProveedores{Apellidos: "Caro Lopez", Correo: "lc@correo.co", Password: "password1A", Nombres: "Luis Alfredo", Organizacion: "Organ1", Token: proveedor.APITOKEN},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 3: Password invalido",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyPutProveedores{Apellidos: "Caro Lopez", Correo: "lc@correo.co", Password: "passwordA", Nombres: "Luis Alfredo", Organizacion: "Organ1", Token: proveedor.APITOKEN},
			statusEsperado:  http.StatusConflict,
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Crear solicitud
			client := &http.Client{}
			cuerpoToken := new(bytes.Buffer)
			json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, error := http.NewRequest("PUT", "http://localhost:8080/v1/PROVEEDORES/"+casoDePrueba.querySolicitud, cuerpoToken)
			solicitudCreada.Header.Set("Content-Type", "application/json; charset=utf-8")

			// Enviar solicitud
			resp, err := client.Do(solicitudCreada)
			if err != nil {
				log.Fatalf("client.Do() failed with '%s'\n", err)
			}

			defer resp.Body.Close()

			if error != nil {
				t.Error(error.Error())
				return
			}

			if resp.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", resp.StatusCode, casoDePrueba.statusEsperado)
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registros de administradores
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

// TestProveedoresDelete Función de prueba unitaria para la función controllers.PROVEEDORES.Delete
func TestPROVEEDORESDelete(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	// - Ingresar proveedores a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'prove1', '', 'prov1@correo.co', ?, NULL)", models.CalcularHashPassword("pa2")).Exec()
	o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO, ORGANIZACION) VALUES (2, 1, '')").Exec()

	// - Obtener token del administrador creado
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso      string
		querySolicitud  string
		cuerpoSolicitud controllers.BodyDeleteProveedores
		statusEsperado  int
		errorEsperado   string
	}{
		{
			nombreCaso:      "Test 1: Eliminación exitosa",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyDeleteProveedores{Token: administrador.APITOKEN},
			statusEsperado:  http.StatusOK,
		},
		{
			nombreCaso:      "Test 2: Sin autorización para eliminar",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyDeleteProveedores{Token: "abcdefghijklmno"},
			statusEsperado:  http.StatusForbidden,
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Crear solicitud
			client := &http.Client{}
			cuerpoToken := new(bytes.Buffer)
			json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, error := http.NewRequest("DELETE", "http://localhost:8080/v1/PROVEEDORES/"+casoDePrueba.querySolicitud, cuerpoToken)
			solicitudCreada.Header.Set("Content-Type", "application/json; charset=utf-8")

			// Enviar solicitud
			resp, err := client.Do(solicitudCreada)
			if err != nil {
				log.Fatalf("client.Do() failed with '%s'\n", err)
			}

			defer resp.Body.Close()

			if error != nil {
				t.Error(error.Error())
				return
			}

			if resp.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", resp.StatusCode, casoDePrueba.statusEsperado)
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registros
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}
