package tests

import (
	"backendmarketplace/controllers"
	"backendmarketplace/models"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"

	"github.com/astaxie/beego/orm"
)

// TestClientesGetOneFunción de prueba unitaria para la función controllers.CLIENTES.GetOne
func TestCLIENTESGetOne(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un usuario de cada rol (administradores, clientes y proveedores) a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'prov1', '', 'prov1@correo.co', ?, NULL)", models.CalcularHashPassword("1234")).Exec()
	o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO, ID_ADMIN_ELIMINACION, ORGANIZACION) VALUES (2, 1, NULL, 'Unal')").Exec()
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (3, 'cli1', '', 'cli1@correo.co', ?, NULL)", models.CalcularHashPassword("12345")).Exec()
	o.Raw("INSERT INTO CLIENTES (ID_USUARIO, ID_ADMIN_VALIDACION) VALUES (3, 1)").Exec()

	// - Generar tokens
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)

	proveedor := models.USUARIOS{Id: 2}
	o.Read(&proveedor)
	proveedor.APITOKEN = models.CalcularApiToken(proveedor)
	o.Update(&proveedor)

	cliente := models.USUARIOS{Id: 3}
	o.Read(&cliente)
	cliente.APITOKEN = models.CalcularApiToken(cliente)
	o.Update(&cliente)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso              string
		statusEsperado          int
		cuerpoRespuestaEsperada *controllers.BodyRespuestaGetOneClientes
		errorEsperado           string
		// cuerpoSolicitud         controllers.BodyGetOneClientes
		token          string
		querySolicitud string
	}{
		{
			nombreCaso:     "Test 1: :id no es un entero",
			statusEsperado: http.StatusBadRequest,
			errorEsperado:  "\"El parámetro :id no es un entero\"",
			// cuerpoSolicitud: controllers.BodyGetOneClientes{TokenUsuario: administrador.APITOKEN},
			token:          administrador.APITOKEN,
			querySolicitud: "NoEntero",
		},
		{
			nombreCaso:     "Test 2: el Token no está en la base de datos",
			statusEsperado: http.StatusForbidden,
			errorEsperado:  "\"Token no está en la base de datos\"",
			// cuerpoSolicitud: controllers.BodyGetOneClientes{TokenUsuario: "TokenQueNoExisteEnBD"},
			token:          "TokenQueNoExiste",
			querySolicitud: "3",
		},
		{
			nombreCaso:     "Test 3: Solicitud de un recurso que no existe",
			statusEsperado: http.StatusNotFound,
			errorEsperado:  "\"Recurso no existe\"",
			// cuerpoSolicitud: controllers.BodyGetOneClientes{TokenUsuario: cliente.APITOKEN},
			token:          cliente.APITOKEN,
			querySolicitud: "100",
		},
		{
			nombreCaso:     "Test 4: lectura exitosa solicitada por un administrador",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetOneClientes{
				Correo: "cli1@correo.co", Nombres: "cli1",
			},
			// cuerpoSolicitud: controllers.BodyGetOneClientes{TokenUsuario: administrador.APITOKEN},
			token:          administrador.APITOKEN,
			querySolicitud: "3",
		},
		{
			nombreCaso:     "Test 5: lectura exitosa solicitada por un proveedor",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetOneClientes{
				Correo: "cli1@correo.co", Nombres: "cli1",
			},
			// cuerpoSolicitud: controllers.BodyGetOneClientes{TokenUsuario: proveedor.APITOKEN},
			token:          proveedor.APITOKEN,
			querySolicitud: "3",
		},
		{
			nombreCaso:     "Test 6: lectura exitosa solicitada por un cliente",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetOneClientes{
				Correo: "cli1@correo.co", Nombres: "cli1",
			},
			// cuerpoSolicitud: controllers.BodyGetOneClientes{TokenUsuario: cliente.APITOKEN},
			token:          cliente.APITOKEN,
			querySolicitud: "3",
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			cliente := &http.Client{}
			//cuerpoToken := new(bytes.Buffer)
			//json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, errorCreacionSolicitud := http.NewRequest("GET", "http://localhost:8080/v1/CLIENTES/"+casoDePrueba.querySolicitud, nil) // cuerpoToken)
			solicitudCreada.Header.Set("Authorization", "Bearer "+casoDePrueba.token)

			if errorCreacionSolicitud != nil {
				t.Errorf("Error al crear la solicitud: %s", errorCreacionSolicitud)
				return
			}

			// Enviar solicitud
			respuestaObtenida, errorEnvioSolicitud := cliente.Do(solicitudCreada)
			if errorEnvioSolicitud != nil {
				t.Errorf("Error al enviar la solicitud: %s", errorEnvioSolicitud)
				return
			}

			defer respuestaObtenida.Body.Close()

			// Revisar código de estado
			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
				return
			}

			// Obtener respuesta
			var cuerpoRespuestaObtenida controllers.BodyRespuestaGetOneClientes
			// Obtener response body como slice de bytes
			cuerpoRespuestaObtenidaBytes, _ := ioutil.ReadAll(respuestaObtenida.Body)
			// Usar el slice de bytes para intentar obtener un struct de respuesta exitosa
			error := json.Unmarshal(cuerpoRespuestaObtenidaBytes, &cuerpoRespuestaObtenida)

			// Si el cuerpo de la respuesta indica que hubo un error
			if error != nil {
				errorRespuestaObtenida := string(cuerpoRespuestaObtenidaBytes)
				// - Si no se esperaba un error
				if casoDePrueba.errorEsperado == "" {
					t.Errorf("Error: no se esperaba un error. Retornó: %s", errorRespuestaObtenida)
				} else if errorRespuestaObtenida != casoDePrueba.errorEsperado {
					// - Si los errores esperado y obtenido son diferentes
					t.Errorf("Error: se obtuvo %s. Se esperaba %s", errorRespuestaObtenida, casoDePrueba.errorEsperado)
				}
			} else {
				// Si el cuerpo de la respuesta obtenida NO es un mensaje de error

				// - Si se esperaba un error
				if casoDePrueba.errorEsperado != "" {
					t.Errorf("Error: se esperaba un error. Se obtuvo %+v", cuerpoRespuestaObtenida)
				} else if !reflect.DeepEqual(*(casoDePrueba.cuerpoRespuestaEsperada), cuerpoRespuestaObtenida) {
					// - Si los body de las respuesta obtenida y esperada son diferentes
					t.Errorf("Error: el cuerpo retornado: %+v, no es el esperado: %+v", cuerpoRespuestaObtenida, *casoDePrueba.cuerpoRespuestaEsperada)
				}
			}
		})
	}

	// Liberación de recursos

	// - Eliminar registros de administradores
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

// TestClientesGetAll Función de prueba unitaria para la función controllers.CLIENTES.GetAll
func TestCLIENTESGetAll(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un usuario de cada rol (administradores, clientes y proveedores) a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'prov1', '', 'prov1@correo.co', ?, NULL)", models.CalcularHashPassword("1234")).Exec()
	o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO, ID_ADMIN_ELIMINACION, ORGANIZACION) VALUES (2, 1, NULL, 'Unal')").Exec()
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (3, 'cli1', '', 'cli1@correo.co', ?, NULL)", models.CalcularHashPassword("12345")).Exec()
	o.Raw("INSERT INTO CLIENTES (ID_USUARIO, ID_ADMIN_VALIDACION) VALUES (3, 1)").Exec()

	// - Generar tokens
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)

	proveedor := models.USUARIOS{Id: 2}
	o.Read(&proveedor)
	proveedor.APITOKEN = models.CalcularApiToken(proveedor)
	o.Update(&proveedor)

	cliente := models.USUARIOS{Id: 3}
	o.Read(&cliente)
	cliente.APITOKEN = models.CalcularApiToken(cliente)
	o.Update(&cliente)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso              string
		statusEsperado          int
		cuerpoRespuestaEsperada *controllers.BodyRespuestaGetAllClientes
		errorEsperado           string
		//cuerpoSolicitud         controllers.BodyGetAllClientes
		token   string
		antes   func()
		despues func()
	}{
		{
			nombreCaso:     "Test 1: lectura exitosa solicitada por un administrador",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllClientes{
				Clientes: []controllers.BodyRespuestaGetOneClientesParaAll{
					{Correo: "cli1@correo.co", Nombres: "cli1", Id: 3},
				},
			},
			token: administrador.APITOKEN,
			//cuerpoSolicitud: controllers.BodyGetAllClientes{TokenUsuario: administrador.APITOKEN},
		},
		{
			nombreCaso:     "Test 2: lectura exitosa solicitada por un proveedor",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllClientes{
				Clientes: []controllers.BodyRespuestaGetOneClientesParaAll{
					{Correo: "cli1@correo.co", Nombres: "cli1", Id: 3},
				},
			},
			token: proveedor.APITOKEN,
			//cuerpoSolicitud: controllers.BodyGetAllClientes{TokenUsuario: proveedor.APITOKEN},
		},
		{
			nombreCaso:     "Test 3: lectura exitosa solicitada por un cliente",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllClientes{
				Clientes: []controllers.BodyRespuestaGetOneClientesParaAll{
					{Correo: "cli1@correo.co", Nombres: "cli1", Id: 3},
				},
			},
			token: cliente.APITOKEN,
			//cuerpoSolicitud: controllers.BodyGetAllClientes{TokenUsuario: cliente.APITOKEN},
		},
		{
			nombreCaso:     "Test 4: lectura exitosa de más de 1 registro",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllClientes{
				Clientes: []controllers.BodyRespuestaGetOneClientesParaAll{
					{Correo: "cli1@correo.co", Nombres: "cli1", Id: 3},
					{Correo: "cli2@correo.co", Nombres: "cli2", Id: 4},
				},
			},
			token: proveedor.APITOKEN,
			//cuerpoSolicitud: controllers.BodyGetAllClientes{TokenUsuario: proveedor.APITOKEN},
			antes: func() {
				// Agregar un nuevo registro de cliente
				o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
					"VALUES (4, 'cli2', '', 'cli2@correo.co', ?, NULL)", models.CalcularHashPassword("12345")).Exec()
				o.Raw("INSERT INTO CLIENTES (ID_USUARIO, ID_ADMIN_VALIDACION) VALUES (4, 1)").Exec()
			},
			despues: func() {
				// Eliminar nuevo registro de cliente
				o.Raw("DELETE FROM CLIENTES WHERE ID_USUARIO = 4").Exec()
				o.Raw("DELETE FROM USUARIOS WHERE ID_USUARIO = 4").Exec()
			},
		},
		{
			nombreCaso:              "Test 5: lectura exitosa de 0 registros",
			statusEsperado:          http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllClientes{},
			token:                   proveedor.APITOKEN,
			//cuerpoSolicitud:         controllers.BodyGetAllClientes{TokenUsuario: proveedor.APITOKEN},
			antes: func() {
				// Eliminar registro de cliente con id 3
				o.Raw("DELETE FROM CLIENTES WHERE ID_USUARIO = 3").Exec()
				o.Raw("DELETE FROM USUARIOS WHERE ID_USUARIO = 3").Exec()
			},
			despues: func() {
				// Volver a agregar registro de cliente con id 3
				o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
					"VALUES (3, 'cli1', '', 'cli1@correo.co', ?, NULL)", models.CalcularHashPassword("12345")).Exec()
				o.Raw("INSERT INTO CLIENTES (ID_USUARIO, ID_ADMIN_VALIDACION) VALUES (3, 1)").Exec()
				// Recalcular token
				cliente := models.USUARIOS{Id: 3}
				o.Read(&cliente)
				cliente.APITOKEN = models.CalcularApiToken(cliente)
				o.Update(&cliente)
			},
		},
		{
			nombreCaso:     "Test 6: Token no está en la base de datos",
			statusEsperado: http.StatusForbidden,
			errorEsperado:  "\"Token no está en la base de datos\"",
			token:          "TokenNoValido",
			//cuerpoSolicitud: controllers.BodyGetAllClientes{TokenUsuario: "TokenNoValido"},
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Preparar ambiente para caso de prueba
			if casoDePrueba.antes != nil {
				casoDePrueba.antes()
			}
			// Cuando la función retorne dejar ambiente como estaba
			if casoDePrueba.despues != nil {
				defer casoDePrueba.despues()
			}

			cliente := &http.Client{}
			//cuerpoToken := new(bytes.Buffer)
			//json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, errorCreacionSolicitud := http.NewRequest("GET", "http://localhost:8080/v1/CLIENTES", nil) //  cuerpoToken
			solicitudCreada.Header.Set("Authorization", "Bearer "+casoDePrueba.token)

			if errorCreacionSolicitud != nil {
				t.Errorf("Error al crear la solicitud: %s", errorCreacionSolicitud)
				return
			}

			// Enviar solicitud
			respuestaObtenida, errorEnvioSolicitud := cliente.Do(solicitudCreada)
			if errorEnvioSolicitud != nil {
				t.Errorf("Error al enviar la solicitud: %s", errorEnvioSolicitud)
				return
			}

			defer respuestaObtenida.Body.Close()

			// Revisar código de estado
			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
				return
			}

			// Obtener respuesta
			var cuerpoRespuestaObtenida controllers.BodyRespuestaGetAllClientes
			// Obtener response body como slice de bytes
			cuerpoRespuestaObtenidaBytes, _ := ioutil.ReadAll(respuestaObtenida.Body)
			// Usar el slice de bytes para intentar obtener un struct de respuesta exitosa
			error := json.Unmarshal(cuerpoRespuestaObtenidaBytes, &cuerpoRespuestaObtenida)

			// Si el cuerpo de la respuesta indica que hubo un error
			if error != nil {
				errorRespuestaObtenida := string(cuerpoRespuestaObtenidaBytes)
				// - Si no se esperaba un error
				if casoDePrueba.errorEsperado == "" {
					t.Errorf("Error: no se esperaba un error. Retornó: %s", errorRespuestaObtenida)
				} else if errorRespuestaObtenida != casoDePrueba.errorEsperado {
					// - Si los errores esperado y obtenido son diferentes
					t.Errorf("Error: se obtuvo %s. Se esperaba %s", errorRespuestaObtenida, casoDePrueba.errorEsperado)
				}
			} else {
				// Si el cuerpo de la respuesta obtenida NO es un mensaje de error

				// - Si se esperaba un error
				if casoDePrueba.errorEsperado != "" {
					t.Errorf("Error: se esperaba un error. Se obtuvo %+v", cuerpoRespuestaObtenida)
				} else if !reflect.DeepEqual(*(casoDePrueba.cuerpoRespuestaEsperada), cuerpoRespuestaObtenida) {
					// - Si los body de las respuesta obtenida y esperada son diferentes
					t.Errorf("Error: el cuerpo retornado: %+v, no es el esperado: %+v", cuerpoRespuestaObtenida, *casoDePrueba.cuerpoRespuestaEsperada)
				}
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registros de administradores
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

// TestCLIENTESPOST Función de prueba unitaria para la función controllers.CLIENTES.Post
func TestCLIENTESPost(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso              string
		datosSolicitud          controllers.BODY_POST_CLIENTES
		statusEsperado          int
		cuerpoRespuestaEsperada *controllers.BODY_RESPUESTA_POST_CLIENTES
		errorEsperado           string
		antes                   func()
		despues                 func()
	}{
		{
			nombreCaso:              "Test 1: creación exitosa",
			datosSolicitud:          controllers.BODY_POST_CLIENTES{Apellidos: "Pardo", Correo: "jm@correo.co", Password: "password1A", Nombres: "Juan Pablo"},
			statusEsperado:          http.StatusCreated,
			cuerpoRespuestaEsperada: &controllers.BODY_RESPUESTA_POST_CLIENTES{Id: 1},
			despues: func() {
				// Eliminar registro usando el correo (es suficiente porque es único)
				var usuarioAEliminar models.USUARIOS
				qs := o.QueryTable(new(models.USUARIOS))
				error := qs.Filter("CORREO", "jm@correo.co").One(&usuarioAEliminar)
				// Si se pudo realizar la creación del cliente
				if error == nil {
					clienteAEliminar := models.CLIENTES{Id: usuarioAEliminar.Id}
					o.Delete(&clienteAEliminar)
					o.Delete(&usuarioAEliminar)
				}
			},
		},

		{
			nombreCaso:     "Test 2: correo ya registrado",
			datosSolicitud: controllers.BODY_POST_CLIENTES{Apellidos: "Martinez", Correo: "jm@correo.co", Password: "pass2", Nombres: "Mario"},
			statusEsperado: http.StatusBadRequest,
			errorEsperado:  "\"Correo ya registrado\"",
			antes: func() {
				// Ingresar registro con el mismo correo
				o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
					"VALUES (1, 'cliente1', '', 'jm@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
				o.Raw("INSERT INTO CLIENTES (ID_USUARIO) VALUES (1)").Exec()
			},
			despues: func() {
				// Eliminar registros ingresados en antes()
				o.Raw("DELETE FROM CLIENTES WHERE ID_USUARIO = 1").Exec()
				o.Raw("DELETE FROM USUARIOS WHERE ID_USUARIO = 1").Exec()
			},
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Preparar ambiente para caso de prueba
			if casoDePrueba.antes != nil {
				casoDePrueba.antes()
			}
			// Cuando la función retorne dejar ambiente como estaba
			if casoDePrueba.despues != nil {
				defer casoDePrueba.despues()
			}

			// Crear solicitud
			cuerpoSolicitud := new(bytes.Buffer)
			json.NewEncoder(cuerpoSolicitud).Encode(casoDePrueba.datosSolicitud)
			respuestaObtenida, error := http.Post("http://localhost:8080/v1/CLIENTES", "application/json", cuerpoSolicitud)

			if error != nil {
				t.Error(error.Error())
				return
			}

			// Siempre se debe cerrar el body de la respuesta luego de usarlo
			defer respuestaObtenida.Body.Close()

			// Revisar código de estado
			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
				return
			}

			// Obtener respuesta
			var cuerpoRespuestaObtenida controllers.BODY_RESPUESTA_POST_CLIENTES
			// Obtener response body como slice de bytes
			cuerpoRespuestaObtenidaBytes, _ := ioutil.ReadAll(respuestaObtenida.Body)
			// Usar el slice de bytes para intentar obtener un struct de respuesta exitosa
			error = json.Unmarshal(cuerpoRespuestaObtenidaBytes, &cuerpoRespuestaObtenida)

			// Si el cuerpo de la respuesta indica que hubo un error
			if error != nil {
				errorRespuestaObtenida := string(cuerpoRespuestaObtenidaBytes)
				// - Si no se esperaba un error
				if casoDePrueba.errorEsperado == "" {
					t.Errorf("Error: no se esperaba un error. Retornó: %s", errorRespuestaObtenida)
				} else if errorRespuestaObtenida != casoDePrueba.errorEsperado {
					// - Si los errores esperado y obtenido son diferentes
					t.Errorf("Error: se obtuvo %s. Se esperaba %s", errorRespuestaObtenida, casoDePrueba.errorEsperado)
				}
			} else {
				// Si el cuerpo de la respuesta obtenida NO es un mensaje de error

				// - Si se esperaba un error
				if casoDePrueba.errorEsperado != "" {
					t.Errorf("Error: se esperaba un error. Se obtuvo %+v", cuerpoRespuestaObtenida)
				} else if *(casoDePrueba.cuerpoRespuestaEsperada) != cuerpoRespuestaObtenida {
					// - Si los body de las respuesta obtenida y esperada son diferentes
					t.Errorf("Error: el cuerpo retornado: %+v, no es el esperado: %+v", cuerpoRespuestaObtenida, *casoDePrueba.cuerpoRespuestaEsperada)
				}
			}

		})
	}

	// Liberación de recursos
	// ..
}

// TestClientesPut Función de prueba unitaria para la función controllers.CLIENTES.Put
func TestCLIENTESPut(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', 'apellido1', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123AAbb5")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	// - Ingresar cliente a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'nombre1', 'apellido2', 'cli1@correo.co', ?, NULL)", models.CalcularHashPassword("passABC2")).Exec()
	o.Raw("INSERT INTO CLIENTES (ID_USUARIO, ID_ADMIN_VALIDACION) VALUES (2, 1)").Exec()

	// - Generar tokens (los usuarios iniciaron sesión)
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)

	cliente := models.USUARIOS{Id: 2}
	o.Read(&cliente)
	cliente.APITOKEN = models.CalcularApiToken(cliente)
	o.Update(&cliente)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso      string
		querySolicitud  string
		cuerpoSolicitud controllers.BodyPutClientes
		statusEsperado  int
		errorEsperado   string
	}{
		{
			nombreCaso:      "Test 1: Solicitud mal construida (índice no es entero)",
			querySolicitud:  "cadena",
			cuerpoSolicitud: controllers.BodyPutClientes{},
			statusEsperado:  http.StatusBadRequest,
		},
		{
			nombreCaso:      "Test 2: No autorizado (token no está en la base de datos)",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyPutClientes{},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 3: No autorizado (token pertenece a un usuario que no es cliente)",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyPutClientes{Token: administrador.APITOKEN},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 4: No autorizado (token pertenece a un cliente que no es dueño de la cuenta)",
			querySolicitud:  "3",
			cuerpoSolicitud: controllers.BodyPutClientes{Token: cliente.APITOKEN},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 5: Apellidos no válidos",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyPutClientes{Apellidos: "Caro", Token: cliente.APITOKEN},
			statusEsperado:  http.StatusConflict,
		},
		{
			nombreCaso:      "Test 6: Correo no válido",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyPutClientes{Correo: "@", Token: cliente.APITOKEN},
			statusEsperado:  http.StatusConflict,
		},
		{
			nombreCaso:      "Test 7: Nombres no válidos",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyPutClientes{Nombres: "Jr", Token: cliente.APITOKEN},
			statusEsperado:  http.StatusConflict,
		},
		{
			nombreCaso:      "Test 8: Password no válido",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyPutClientes{Password: "pass", Token: cliente.APITOKEN},
			statusEsperado:  http.StatusConflict,
		},
		{
			nombreCaso:     "Test 9: Modificación exitosa",
			querySolicitud: "2",
			cuerpoSolicitud: controllers.BodyPutClientes{Apellidos: "NuevoApellido", Correo: "nuevoclicorreo@correo.co",
				Nombres: "NuevoNombre", Password: "passWORD1", Token: cliente.APITOKEN},
			statusEsperado: http.StatusOK,
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Crear solicitud
			cliente := &http.Client{}
			cuerpoToken := new(bytes.Buffer)
			json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, errorCreacionSolicitud := http.NewRequest("PUT", "http://localhost:8080/v1/CLIENTES/"+casoDePrueba.querySolicitud, cuerpoToken)

			if errorCreacionSolicitud != nil {
				t.Errorf("Error al crear la solicitud: %s", errorCreacionSolicitud)
				return
			}

			solicitudCreada.Header.Set("Content-Type", "application/json; charset=utf-8")

			// Enviar solicitud
			respuestaObtenida, errorEnvioSolicitud := cliente.Do(solicitudCreada)
			if errorEnvioSolicitud != nil {
				t.Errorf("Error al enviar la solicitud: %s", errorEnvioSolicitud)
				return
			}

			defer respuestaObtenida.Body.Close()

			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registros
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

// TestClientesDelete Función de prueba unitaria para la función controllers.CLIENTES.Delete
func TestCLIENTESDelete(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	// - Ingresar alguien que no sea usuario a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'cli1', '', 'cli1@correo.co', ?, NULL)", models.CalcularHashPassword("213")).Exec()
	o.Raw("INSERT INTO CLIENTES (ID_USUARIO, ID_ADMIN_VALIDACION) VALUES (2, 1)").Exec()

	// - Generar tokens
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)

	cliente := models.USUARIOS{Id: 2}
	o.Read(&cliente)
	cliente.APITOKEN = models.CalcularApiToken(cliente)
	o.Update(&cliente)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso      string
		querySolicitud  string
		cuerpoSolicitud controllers.BodyDeleteClientes
		statusEsperado  int
	}{
		{
			nombreCaso:      "Test 1: :id no es entero",
			querySolicitud:  "string",
			cuerpoSolicitud: controllers.BodyDeleteClientes{Token: administrador.APITOKEN},
			statusEsperado:  http.StatusBadRequest,
		},
		{
			nombreCaso:      "Test 2: Token no está en la base de datos",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyDeleteClientes{Token: "abcdefghijklmno"},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 3: Token no es de un administrador",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyDeleteClientes{Token: cliente.APITOKEN},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 4: No existe recurso a eliminar",
			querySolicitud:  "3",
			cuerpoSolicitud: controllers.BodyDeleteClientes{Token: administrador.APITOKEN},
			statusEsperado:  http.StatusNotFound,
		},
		{
			nombreCaso:      "Test 5: Eliminación exitosa",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyDeleteClientes{Token: administrador.APITOKEN},
			statusEsperado:  http.StatusOK,
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Crear solicitud
			cliente := &http.Client{}
			cuerpoToken := new(bytes.Buffer)
			json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, errorCreacionSolicitud := http.NewRequest("DELETE", "http://localhost:8080/v1/CLIENTES/"+casoDePrueba.querySolicitud, cuerpoToken)

			if errorCreacionSolicitud != nil {
				t.Errorf("Error al crear la solicitud: %s", errorCreacionSolicitud)
				return
			}

			solicitudCreada.Header.Set("Content-Type", "application/json; charset=utf-8")

			// Enviar solicitud
			respuestaObtenida, errorEnvioSolicitud := cliente.Do(solicitudCreada)
			if errorEnvioSolicitud != nil {
				t.Errorf("Error al enviar la solicitud: %s", errorEnvioSolicitud)
				return
			}

			defer respuestaObtenida.Body.Close()

			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registros
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()

}
