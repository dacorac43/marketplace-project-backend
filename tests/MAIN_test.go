package tests

import (
	"fmt"
	"os"
	"testing"

	"github.com/astaxie/beego/orm"
	"github.com/go-ini/ini"
)

// Obtener configuración de ambiente "test"
func obtenerConecccion() string {

	// Cargar archivo de configuración
	conf, err := ini.Load("../conf/conexbasedatos.conf")
	if err != nil {
		fmt.Printf("No se pudo leer el archivo de configuraciń: %v", err)
		os.Exit(1)
	}

	// Leer datos de configuración
	usuario := conf.Section("").Key("mysqluser").String()
	pass := conf.Section("").Key("mysqlpass").String()
	db := conf.Section("test").Key("mysqldb").String()

	return usuario + ":" + pass + "@tcp(127.0.0.1:3306)/" + db
}

// Llevar base de datos al estado inicial antes de ejecutar las pruebas
func llevarBaseDeDatosAEstadoInicial() {
	o := orm.NewOrm()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

// Registrar base de datos de acuerdo a configuración antes de ejecutar los tests y llevar base de
// datos a estado inicial
func setup() {
	orm.RegisterDataBase("default", "mysql", obtenerConecccion())
	llevarBaseDeDatosAEstadoInicial()
}

// Colocar en esta función lo que deba ejecutarse después de los tests
func shutdown() {
	// Llevar base de datos al estado final
	o := orm.NewOrm()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

func TestMain(m *testing.M) {
	setup()
	// Ejecutar tests
	exitCode := m.Run()
	shutdown()
	os.Exit(exitCode)
}
