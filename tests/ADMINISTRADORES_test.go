package tests

import (
	"backendmarketplace/controllers"
	"backendmarketplace/models"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	_ "backendmarketplace/routers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

// TestAdministradoresGetOne Función de prueba unitaria para la función controllers.ADMINISTRADORES.GetOne
func TestADMINISTRADORESGetOne(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un administrador
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso              string
		statusEsperado          int
		cuerpoRespuestaEsperada *controllers.BodyRespuestaGetOneAdministradores
		errorEsperado           string
		querySolicitud          string
	}{
		{
			nombreCaso:     "Test 1: :id no es un entero",
			statusEsperado: http.StatusBadRequest,
			errorEsperado:  "\"El parámetro :id no es un entero\"",
			querySolicitud: "NoEntero",
		},
		{
			nombreCaso:     "Test 2: Solicitud de un recurso que no existe",
			statusEsperado: http.StatusNotFound,
			errorEsperado:  "\"Recurso no existe\"",
			querySolicitud: "100",
		},
		{
			nombreCaso:     "Test 3: Lectura exitosa",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetOneAdministradores{
				Correo: "admin1@correo.co", Nombres: "admin1",
			},
			querySolicitud: "1",
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			cliente := &http.Client{}
			solicitudCreada, errorCreacionSolicitud := http.NewRequest("GET", "http://localhost:8080/v1/ADMINISTRADORES/"+casoDePrueba.querySolicitud, nil)

			if errorCreacionSolicitud != nil {
				t.Errorf("Error al crear la solicitud: %s", errorCreacionSolicitud)
				return
			}

			// Enviar solicitud
			respuestaObtenida, errorEnvioSolicitud := cliente.Do(solicitudCreada)
			if errorEnvioSolicitud != nil {
				t.Errorf("Error al enviar la solicitud: %s", errorEnvioSolicitud)
				return
			}

			defer respuestaObtenida.Body.Close()

			// Revisar código de estado
			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
				return
			}

			// Obtener respuesta
			var cuerpoRespuestaObtenida controllers.BodyRespuestaGetOneAdministradores
			// Obtener response body como slice de bytes
			cuerpoRespuestaObtenidaBytes, _ := ioutil.ReadAll(respuestaObtenida.Body)
			// Usar el slice de bytes para intentar obtener un struct de respuesta exitosa
			error := json.Unmarshal(cuerpoRespuestaObtenidaBytes, &cuerpoRespuestaObtenida)

			// Si el cuerpo de la respuesta indica que hubo un error
			if error != nil {
				errorRespuestaObtenida := string(cuerpoRespuestaObtenidaBytes)
				// - Si no se esperaba un error
				if casoDePrueba.errorEsperado == "" {
					t.Errorf("Error: no se esperaba un error. Retornó: %s", errorRespuestaObtenida)
				} else if errorRespuestaObtenida != casoDePrueba.errorEsperado {
					// - Si los errores esperado y obtenido son diferentes
					t.Errorf("Error: se obtuvo %s. Se esperaba %s", errorRespuestaObtenida, casoDePrueba.errorEsperado)
				}
			} else {
				// Si el cuerpo de la respuesta obtenida NO es un mensaje de error

				// - Si se esperaba un error
				if casoDePrueba.errorEsperado != "" {
					t.Errorf("Error: se esperaba un error. Se obtuvo %+v", cuerpoRespuestaObtenida)
				} else if !reflect.DeepEqual(*(casoDePrueba.cuerpoRespuestaEsperada), cuerpoRespuestaObtenida) {
					// - Si los body de las respuesta obtenida y esperada son diferentes
					t.Errorf("Error: el cuerpo retornado: %+v, no es el esperado: %+v", cuerpoRespuestaObtenida, *casoDePrueba.cuerpoRespuestaEsperada)
				}
			}
		})
	}

	// Liberación de recursos

	// - Eliminar registros de administradores
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

// TestAdministradoresGetAll Función de prueba unitaria para la función controllers.ADMINISTRADORES.GetAll
func TestADMINISTRADORESGetAll(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'admin2', '', 'admin2@correo.co', ?, NULL)", models.CalcularHashPassword("1234")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (2)").Exec()

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso              string
		querySolicitud          string
		statusEsperado          int
		cuerpoRespuestaEsperada *controllers.BodyRespuestaGetAllAdministradores
		errorEsperado           string
	}{
		{
			nombreCaso:     "Test 1: lectura exitosa",
			querySolicitud: "",
			statusEsperado: http.StatusOK,
			cuerpoRespuestaEsperada: &controllers.BodyRespuestaGetAllAdministradores{
				Administradores: []controllers.BodyRespuestaGetOneAdministradoresParaAll{
					{Correo: "admin1@correo.co", Nombres: "admin1", Id: 1},
					{Correo: "admin2@correo.co", Nombres: "admin2", Id: 2},
				},
			},
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Crear solicitud
			solicitudCreada, error := http.NewRequest("GET", "/v1/ADMINISTRADORES/"+casoDePrueba.querySolicitud, nil)
			registroRespuesta := httptest.NewRecorder()

			// Enviar solicitud
			beego.BeeApp.Handlers.ServeHTTP(registroRespuesta, solicitudCreada)

			if error != nil {
				t.Error(error.Error())
				return
			}

			// Revisar código de estado
			if registroRespuesta.Code != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", registroRespuesta.Code, casoDePrueba.statusEsperado)
				return
			}

			// Obtener respuesta
			var cuerpoRespuestaObtenida controllers.BodyRespuestaGetAllAdministradores
			// Obtener response body como slice de bytes
			cuerpoRespuestaObtenidaBytes := registroRespuesta.Body.Bytes()
			// Usar el slice de bytes para intentar obtener un struct de respuesta exitosa
			error = json.Unmarshal(cuerpoRespuestaObtenidaBytes, &cuerpoRespuestaObtenida)

			// Si el cuerpo de la respuesta indica que hubo un error
			if error != nil {
				errorRespuestaObtenida := string(cuerpoRespuestaObtenidaBytes)
				// - Si no se esperaba un error
				if casoDePrueba.errorEsperado == "" {
					t.Errorf("Error: no se esperaba un error. Retornó: %s", errorRespuestaObtenida)
				} else if errorRespuestaObtenida != casoDePrueba.errorEsperado {
					// - Si los errores esperado y obtenido son diferentes
					t.Errorf("Error: se obtuvo %s. Se esperaba %s", errorRespuestaObtenida, casoDePrueba.errorEsperado)
				}
			} else {
				// Si el cuerpo de la respuesta obtenida NO es un mensaje de error

				// - Si se esperaba un error
				if casoDePrueba.errorEsperado != "" {
					t.Errorf("Error: se esperaba un error. Se obtuvo %+v", cuerpoRespuestaObtenida)
				} else if !reflect.DeepEqual(*(casoDePrueba.cuerpoRespuestaEsperada), cuerpoRespuestaObtenida) {
					// - Si los body de las respuesta obtenida y esperada son diferentes
					t.Errorf("Error: el cuerpo retornado: %+v, no es el esperado: %+v", cuerpoRespuestaObtenida, *casoDePrueba.cuerpoRespuestaEsperada)
				}
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registros de administradores
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}

// TestAdministradoresPost Función de prueba unitaria para la función controllers.ADMINISTRADORES.Post
func TestADMINISTRADORESPost(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin', '', 'admin@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	// - Obtener token del administrador creado
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso              string
		datosSolicitud          controllers.BODY_POST_ADMINISTRADORES
		statusEsperado          int
		cuerpoRespuestaEsperada *controllers.BODY_RESPUESTA_POST_ADMINISTRADORES
		errorEsperado           string
		antes                   func()
		despues                 func()
	}{
		{
			nombreCaso:              "Test 1: creación exitosa",
			datosSolicitud:          controllers.BODY_POST_ADMINISTRADORES{Apellidos: "Pardo", Correo: "jm@correo.co", Password: "password1A", Nombres: "Juan Pablo", Token: administrador.APITOKEN},
			statusEsperado:          http.StatusCreated,
			cuerpoRespuestaEsperada: &controllers.BODY_RESPUESTA_POST_ADMINISTRADORES{Id: 2},
			despues: func() {
				// Eliminar registro usando el correo (es suficiente porque es único)
				var usuarioAEliminar models.USUARIOS
				qs := o.QueryTable(new(models.USUARIOS))
				error := qs.Filter("CORREO", "jm@correo.co").One(&usuarioAEliminar)
				// Si se pudo realizar la creación del administrador
				if error == nil {
					administradorAEliminar := models.ADMINISTRADORES{Id: usuarioAEliminar.Id}
					o.Delete(&administradorAEliminar)
					o.Delete(&usuarioAEliminar)
				}
			},
		},

		{
			nombreCaso:     "Test 2: correo ya registrado",
			datosSolicitud: controllers.BODY_POST_ADMINISTRADORES{Apellidos: "Martinez", Correo: "jm@correo.co", Password: "pass2", Nombres: "Mario", Token: administrador.APITOKEN},
			statusEsperado: http.StatusBadRequest,
			errorEsperado:  "\"Correo ya registrado\"",
			antes: func() {
				// Ingresar registro con el mismo correo
				o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
					"VALUES (2, 'admin2', '', 'jm@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
				o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (2)").Exec()
			},
			despues: func() {
				// Eliminar registros ingresados en antes()
				o.Raw("DELETE FROM ADMINISTRADORES WHERE ID_ADMIN = 2").Exec()
				o.Raw("DELETE FROM USUARIOS WHERE ID_USUARIO = 2").Exec()
			},
		},

		{
			nombreCaso:     "Test 3: token no válido",
			datosSolicitud: controllers.BODY_POST_ADMINISTRADORES{Apellidos: "Martinez", Correo: "jm@correo.co", Password: "pass2", Nombres: "Mario", Token: ""},
			statusEsperado: http.StatusForbidden,
			errorEsperado:  "\"Token no válido\"",
			despues: func() {
				// Eliminar registros ingresados si es que incorrectamente se llegaron a registrar
				o.Raw("DELETE FROM ADMINISTRADORES WHERE ID_ADMIN = 2").Exec()
				o.Raw("DELETE FROM USUARIOS WHERE ID_USUARIO = 2").Exec()
			},
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Preparar ambiente para caso de prueba
			if casoDePrueba.antes != nil {
				casoDePrueba.antes()
			}
			// Cuando la función retorne dejar ambiente como estaba
			if casoDePrueba.despues != nil {
				defer casoDePrueba.despues()
			}

			// Crear solicitud
			cuerpoSolicitud := new(bytes.Buffer)
			json.NewEncoder(cuerpoSolicitud).Encode(casoDePrueba.datosSolicitud)
			respuestaObtenida, error := http.Post("http://localhost:8080/v1/ADMINISTRADORES", "application/json", cuerpoSolicitud)

			if error != nil {
				t.Error(error.Error())
				return
			}

			// Siempre se debe cerrar el body de la respuesta luego de usarlo
			defer respuestaObtenida.Body.Close()

			// Revisar código de estado
			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
				return
			}

			// Obtener respuesta
			var cuerpoRespuestaObtenida controllers.BODY_RESPUESTA_POST_ADMINISTRADORES
			// Obtener response body como slice de bytes
			cuerpoRespuestaObtenidaBytes, _ := ioutil.ReadAll(respuestaObtenida.Body)
			// Usar el slice de bytes para intentar obtener un struct de respuesta exitosa
			error = json.Unmarshal(cuerpoRespuestaObtenidaBytes, &cuerpoRespuestaObtenida)

			// Si el cuerpo de la respuesta indica que hubo un error
			if error != nil {
				errorRespuestaObtenida := string(cuerpoRespuestaObtenidaBytes)
				// - Si no se esperaba un error
				if casoDePrueba.errorEsperado == "" {
					t.Errorf("Error: no se esperaba un error. Retornó: %s", errorRespuestaObtenida)
				} else if errorRespuestaObtenida != casoDePrueba.errorEsperado {
					// - Si los errores esperado y obtenido son diferentes
					t.Errorf("Error: se obtuvo %s. Se esperaba %s", errorRespuestaObtenida, casoDePrueba.errorEsperado)
				}
			} else {
				// Si el cuerpo de la respuesta obtenida NO es un mensaje de error

				// - Si se esperaba un error
				if casoDePrueba.errorEsperado != "" {
					t.Errorf("Error: se esperaba un error. Se obtuvo %+v", cuerpoRespuestaObtenida)
				} else if *(casoDePrueba.cuerpoRespuestaEsperada) != cuerpoRespuestaObtenida {
					// - Si los body de las respuesta obtenida y esperada son diferentes
					t.Errorf("Error: el cuerpo retornado: %+v, no es el esperado: %+v", cuerpoRespuestaObtenida, *casoDePrueba.cuerpoRespuestaEsperada)
				}
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registro del administrador
	o.Raw("DELETE FROM ADMINISTRADORES WHERE ID_ADMIN = ?", administrador.Id).Exec()
	o.Delete(&administrador)
}

// TestAdministradoresDelete Función de prueba unitaria para la función controllers.ADMINISTRADORES.Delete
func TestADMINISTRADORESDelete(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar administradores a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', '', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'admin2', '', 'admin2@correo.co', ?, NULL)", models.CalcularHashPassword("321")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (2)").Exec()

	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (3, 'admin3', '', 'admin3@correo.co', ?, NULL)", models.CalcularHashPassword("321")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (3)").Exec()

	// - Ingresar cliente a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (4, 'cli1', '', 'cli1@correo.co', ?, NULL)", models.CalcularHashPassword("213")).Exec()
	o.Raw("INSERT INTO CLIENTES (ID_USUARIO) VALUES (4)").Exec()

	// - Generar tokens
	administrador1 := models.USUARIOS{Id: 1}
	o.Read(&administrador1)
	administrador1.APITOKEN = models.CalcularApiToken(administrador1)
	o.Update(&administrador1)

	administrador3 := models.USUARIOS{Id: 3}
	o.Read(&administrador3)
	administrador3.APITOKEN = models.CalcularApiToken(administrador3)
	o.Update(&administrador3)

	cliente := models.USUARIOS{Id: 4}
	o.Read(&cliente)
	cliente.APITOKEN = models.CalcularApiToken(cliente)
	o.Update(&cliente)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso      string
		querySolicitud  string
		cuerpoSolicitud controllers.BodyDeleteAdministradores
		statusEsperado  int
		errorEsperado   string
	}{
		{
			nombreCaso:      "Test 1: Eliminación exitosa",
			querySolicitud:  "1",
			cuerpoSolicitud: controllers.BodyDeleteAdministradores{Token: administrador1.APITOKEN},
			statusEsperado:  http.StatusOK,
		},
		{
			nombreCaso:      "Test 2: Token no está en la base de datos",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyDeleteAdministradores{Token: "abcdefghijklmno"},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 3: Token no es de un administrador",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyDeleteAdministradores{Token: cliente.APITOKEN},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 4: Token es de un administrador que no es dueño de la cuenta",
			querySolicitud:  "2",
			cuerpoSolicitud: controllers.BodyDeleteAdministradores{Token: administrador3.APITOKEN},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 5: Id no es entero",
			querySolicitud:  "string",
			cuerpoSolicitud: controllers.BodyDeleteAdministradores{Token: administrador3.APITOKEN},
			statusEsperado:  http.StatusBadRequest,
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Crear solicitud
			cliente := &http.Client{}
			cuerpoToken := new(bytes.Buffer)
			json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, errorCreacionSolicitud := http.NewRequest("DELETE", "http://localhost:8080/v1/ADMINISTRADORES/"+casoDePrueba.querySolicitud, cuerpoToken)

			if errorCreacionSolicitud != nil {
				t.Errorf("Error al crear la solicitud: %s", errorCreacionSolicitud)
				return
			}

			solicitudCreada.Header.Set("Content-Type", "application/json; charset=utf-8")

			// Enviar solicitud
			respuestaObtenida, errorEnvioSolicitud := cliente.Do(solicitudCreada)
			if errorEnvioSolicitud != nil {
				t.Errorf("Error al enviar la solicitud: %s", errorEnvioSolicitud)
				return
			}

			defer respuestaObtenida.Body.Close()

			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registros
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM CLIENTES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()

}

// TestAdministradoresPut Función de prueba unitaria para la función controllers.ADMINISTRADORES.Put
func TestADMINISTRADORESPut(t *testing.T) {

	// Creación de recursos necesarios para todos los subtests

	// - Resetear el autoincrement
	o := orm.NewOrm()
	o.Raw("ALTER TABLE USUARIOS AUTO_INCREMENT = 1").Exec()

	// - Ingresar un administrador a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (1, 'admin1', 'apellido1', 'admin1@correo.co', ?, NULL)", models.CalcularHashPassword("123AAbb5")).Exec()
	o.Raw("INSERT INTO ADMINISTRADORES (ID_ADMIN) VALUES (1)").Exec()

	// - Ingresar proveedor a la base de datos
	o.Raw("INSERT INTO USUARIOS (ID_USUARIO, NOMBRES, APELLIDOS, CORREO, HASH_PASSWORD, API_TOKEN) "+
		"VALUES (2, 'prove1', 'apellido2', 'prov1@correo.co', ?, NULL)", models.CalcularHashPassword("passABC2")).Exec()
	o.Raw("INSERT INTO PROVEEDORES (ID_PROVEEDOR, ID_ADMIN_REGISTRO, ORGANIZACION) VALUES (2, 1, 'Org123')").Exec()

	// - Generar tokens (los usuarios iniciaron sesión)
	administrador := models.USUARIOS{Id: 1}
	o.Read(&administrador)
	administrador.APITOKEN = models.CalcularApiToken(administrador)
	o.Update(&administrador)

	proveedor := models.USUARIOS{Id: 2}
	o.Read(&proveedor)
	proveedor.APITOKEN = models.CalcularApiToken(proveedor)
	o.Update(&proveedor)

	// Casos de prueba
	casosDePrueba := []struct {
		nombreCaso      string
		querySolicitud  string
		cuerpoSolicitud controllers.BodyPutAdministradores
		statusEsperado  int
		errorEsperado   string
	}{
		{
			nombreCaso:      "Test 1: Solicitud mal construida (índice no es entero)",
			querySolicitud:  "cadena",
			cuerpoSolicitud: controllers.BodyPutAdministradores{},
			statusEsperado:  http.StatusBadRequest,
		},
		{
			nombreCaso:      "Test 2: No autorizado (token no está en la base de datos)",
			querySolicitud:  "1",
			cuerpoSolicitud: controllers.BodyPutAdministradores{},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 3: No autorizado (token pertenece a un usuario que no es admnistrador)",
			querySolicitud:  "1",
			cuerpoSolicitud: controllers.BodyPutAdministradores{Token: proveedor.APITOKEN},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 4: No autorizado (token pertenece a un administrador que no es dueño de la cuenta)",
			querySolicitud:  "3",
			cuerpoSolicitud: controllers.BodyPutAdministradores{Token: administrador.APITOKEN},
			statusEsperado:  http.StatusForbidden,
		},
		{
			nombreCaso:      "Test 5: Apellidos no válidos",
			querySolicitud:  "1",
			cuerpoSolicitud: controllers.BodyPutAdministradores{Apellidos: "Caro", Token: administrador.APITOKEN},
			statusEsperado:  http.StatusConflict,
		},
		{
			nombreCaso:      "Test 6: Correo no válido",
			querySolicitud:  "1",
			cuerpoSolicitud: controllers.BodyPutAdministradores{Correo: "@", Token: administrador.APITOKEN},
			statusEsperado:  http.StatusConflict,
		},
		{
			nombreCaso:      "Test 7: Nombres no válidos",
			querySolicitud:  "1",
			cuerpoSolicitud: controllers.BodyPutAdministradores{Nombres: "Jr", Token: administrador.APITOKEN},
			statusEsperado:  http.StatusConflict,
		},
		{
			nombreCaso:      "Test 8: Password no válido",
			querySolicitud:  "1",
			cuerpoSolicitud: controllers.BodyPutAdministradores{Password: "pass", Token: administrador.APITOKEN},
			statusEsperado:  http.StatusConflict,
		},
		{
			nombreCaso:     "Test 9: Modificación exitosa",
			querySolicitud: "1",
			cuerpoSolicitud: controllers.BodyPutAdministradores{Apellidos: "NuevoApellido", Correo: "nuevoadmcorreo@correo.co",
				Nombres: "NuevoNombre", Password: "passWORD1", Token: administrador.APITOKEN},
			statusEsperado: http.StatusOK,
		},
	}

	for _, casoDePrueba := range casosDePrueba {

		t.Run(casoDePrueba.nombreCaso, func(t *testing.T) {

			// Crear solicitud
			cliente := &http.Client{}
			cuerpoToken := new(bytes.Buffer)
			json.NewEncoder(cuerpoToken).Encode(casoDePrueba.cuerpoSolicitud)
			solicitudCreada, errorCreacionSolicitud := http.NewRequest("PUT", "http://localhost:8080/v1/ADMINISTRADORES/"+casoDePrueba.querySolicitud, cuerpoToken)

			if errorCreacionSolicitud != nil {
				t.Errorf("Error al crear la solicitud: %s", errorCreacionSolicitud)
				return
			}

			solicitudCreada.Header.Set("Content-Type", "application/json; charset=utf-8")

			// Enviar solicitud
			respuestaObtenida, errorEnvioSolicitud := cliente.Do(solicitudCreada)
			if errorEnvioSolicitud != nil {
				t.Errorf("Error al enviar la solicitud: %s", errorEnvioSolicitud)
				return
			}

			defer respuestaObtenida.Body.Close()

			if respuestaObtenida.StatusCode != casoDePrueba.statusEsperado {
				t.Errorf("Error: se obtuvo un código %d. Se esperaba %d", respuestaObtenida.StatusCode, casoDePrueba.statusEsperado)
			}

		})
	}

	// Liberación de recursos

	// - Eliminar registros de administradores
	o.Raw("DELETE FROM PROVEEDORES").Exec()
	o.Raw("DELETE FROM ADMINISTRADORES").Exec()
	o.Raw("DELETE FROM USUARIOS").Exec()
}
